(function () {
    'use strict'

    angular.module('isamedia-app')
        .controller('VideosController', Controller);

    /* global angular */
    function Controller($scope, Content, Device, FlickityService, Modal, $timeout) {
        var that = this;

        var enableSelectOrOpen = false;

        var modalOptions = {
            templateUrl: 'isamedia-app/video.modal.tpl.html',
            controller: 'VideoController as videoCtrl',
            scope: $scope
        };

        that.init = function() {
            Content.getVideos().then(function(videos) {
                that.categories = videos;
            });
        }
        that.init();

        that.flickityOptions = {
            cellSelector: '.video',
            cellAlign: 'left',
            contain: true,
            percentPosition: true,
            pageDots: false,
            prevNextButtons: false,
            wrapAround: false,
            selectedAttraction: 0.01,
            friction: 0.15
        };

        that.select = function (id, index) {
            if(enableSelectOrOpen) {
                FlickityService.select(id, index);
            }
        }

        that.open = function (id, video, force) {
            if(enableSelectOrOpen || force) {
                modalOptions.resolve = {
                    video: video
                };
                Modal.open(modalOptions);
            }
        }

        that.openCurrent = function() {
            var found = false;
            setShowLoader(true);
            Device.getCurrentMediaId().then(function(id) {
                Content.getVideos().then(function(videos) {
                    for(var i = 0; i < that.categories.length; i++) {
                        var category = that.categories[i];
                        var videos = category.videos;
                        for(var v = 0; v < videos.length; v++) {
                            var video = videos[v];
                            if(video.id == id) {
                                found = true;
                                that.open(null, video, true);
                                break;
                            }
                        }
                        if(found) {
                            break;
                        }
                    }
                    setShowLoader(false);
                });
            });
        }

        that.enableSelectOrOpen = function () {
            enableSelectOrOpen = true;
            $timeout(function() {
                enableSelectOrOpen = false;
            }, 200);
        }

        function setShowLoader(show) {
            that.showLoader = show;
        }

    }

})();