(function () {
    'use strict'

    angular.module('isamedia-app')
        .controller('VideoController', Controller);

    /* global angular */
    function Controller(video, MENUVIDEO, Device, Message, MESSAGES, SLIDEROPTIONS, VideoLanguage, $q, $timeout, $interval, $rootScope) {
        var that = this;
        var increasePlaybackSliderTimer = null;
        var updatePlaybackSliderTimer = null;

        that.video = video;
        that.menu = angular.copy(MENUVIDEO);
        that.activeMenu = null;
        that.devices = null;
        that.language = null;
        that.currentIconState = "LOAD";
        that.showLoader = false;
        that.sliderOptions = angular.copy(SLIDEROPTIONS);
        that.sliderOptions.onEnd = setInitialTime;
        that.sliderOptions.ceil = that.video.durationInSeconds;
        that.sliderOptions.translate = translateSliderValue;

        that.setActiveMenuByName = function(name) {
            angular.forEach(that.menu, function(menu) {
                if(menu.name == name) {
                    that.setActiveMenu(menu);
                }
            });
        }

        that.setActiveMenu = function(menu) {
            if(that.activeMenu != null) {
                that.activeMenu.active = false;
            }
            that.activeMenu = menu;
            menu.active = true;
        }

        that.initMenu = function(menu) {
            if(menu.active) {
                that.setActiveMenu(menu);
            }
        }

        that.initDevice = function() {
            if(!that.devices) {
                setShowLoader(true);
            }
            Device.refresh().success(function() {
                setShowLoader(false);
                that.devices = Device.get();
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.deviceRefreshError);
            });;
        }

        that.initLanguage = function() {
            if(!that.language) {
                setShowLoader(true);
            }
            VideoLanguage.get(that.video.id).success(function(language) {
                setShowLoader(false);
                that.language = language;
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.videoLanguageError);
            });
        }
        that.initLanguage();

        that.load = function() {
            setShowLoader(true);
            Device.getConnected().success(function(device) {
                if(!device) {
                    that.setActiveMenuByName('device');
                } else {
                    setShowLoader(true);
                    Device.load(that.video, that.language).success(function() {
                        setCurrentIconState();
                        activeVideoMenu();
                        startPlaybackTimer();
                    }).error(function() {
                        setShowLoader(false);
                        Message.show(MESSAGES.loadDeviceError);
                    });
                }
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectedDeviceError);
            });
        }

        that.play = function() {
            setShowLoader(true);
            Device.play().success(function() {
                setCurrentIconState();
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.playDeviceError);
            });
        }

        that.pause = function() {
            setShowLoader(true);
            Device.pause().success(function() {
                setCurrentIconState();
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.pauseDeviceError);
            });
        }

        that.loadOnDevice = function(device) {
            setShowLoader(true);
            Device.connect(device).success(function() {
                setShowLoader(true);
                Device.load(that.video, that.language).success(function() {
                    setCurrentIconState();
                    activeVideoMenu();
                    startPlaybackTimer();
                }).error(function() {
                    setShowLoader(false);
                    Message.show(MESSAGES.loadDeviceError);
                });
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectDeviceError);
            });
        }

        that.selectLanguageAudio = function(audio) {
            angular.forEach(that.language.audio, function(audio) {
                if(audio.selected) {
                    audio.selected = undefined;
                }
            });
            audio.selected = true;
        }

        that.selectLanguageSubtitle = function(subtitle) {
            angular.forEach(that.language.subtitle, function(subtitle) {
                if(subtitle.selected) {
                    subtitle.selected = undefined;
                }
            });
            subtitle.selected = true;
        }

        Device.onLoad(function() {
            setShowLoader(false);
        });

        Device.onPlay(function() {
            setShowLoader(false);
        });

        Device.onPause(function() {
            setShowLoader(false);
        });

        Device.onConnect(function() {
            setShowLoader(false);
        });

        function setShowLoader(show) {
            that.showLoader = show;
        }

        setCurrentIconState();
        function setCurrentIconState() {
            Device.getConnected().success(function(device) {
                that.currentIconState = "LOAD";
                if(device && device.status.mediaId == that.video.id) {
                    if(device.status.state == "PLAYING") {
                        startPlaybackTimer();
                        fixPlaybackSlider();
                        that.currentIconState = "PAUSE"
                    } else if(device.status.state == "PAUSED") {
                        startPlaybackTimer();
                        fixPlaybackSlider();
                        that.currentIconState = "PLAY"
                    }
                }
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectedDeviceError);
            });
        }

        function isCurrentMedia() {
            var deferred = $q.defer();

            Device.getConnected().success(function(device) {
                if(device && device.status.mediaId == that.video.id) {
                    deferred.resolve()
                }
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectedDeviceError);
            });

            return deferred.promise;
        }

        function isPlaying() {
            return that.currentIconState == 'PAUSE';
        }

        function fixPlaybackSlider() {
            $timeout(function() {
                $rootScope.$broadcast('rzSliderForceRender');
            }, 1000);
        }

        function startPlaybackTimer() {
            $interval.cancel(increasePlaybackSliderTimer);
            $interval.cancel(updatePlaybackSliderTimer);
            increasePlaybackSliderTimer = $interval(increasePlaybackSlider, 1000);
            updatePlaybackSliderTimer = $interval(updatePlaybackSlider, 10000);
        }

        function increasePlaybackSlider() {
            if(isPlaying() && that.video.durationInSeconds > that.language.discardInitialSeconds) {
                that.language.discardInitialSeconds++;
            }
        }

        function updatePlaybackSlider () {
            Device.getConnected().success(function(device) {
                if(device && device.status.mediaId == that.video.id) {
                    that.language.discardInitialSeconds = device.status.currentTime;
                }
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectedDeviceError);
            });
        }

        function translateSliderValue(value, sliderId, label) {
            if(!value) {
                value = 0;
            }
            if(label == "model") {
                value = new Date(value * 1000).toISOString().substr(11, 8);
            }
            return value;
        }

        function setInitialTime() {
            that.load();
        }

        function activeVideoMenu() {
            that.setActiveMenuByName('video');
        }

        function activeLanguageMenu() {
            that.setActiveMenuByName('language');
        }

        function activeDeviceMenu() {
            that.setActiveMenuByName('device');
        }

    }

})();