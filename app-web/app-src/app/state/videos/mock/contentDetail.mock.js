(function () {
    'use strict'

    angular.module('isamedia-app')
        .service('contentDetailMock', Mock);

    /* @ngInject */
    function Mock(basicMock, urlUtil, SETTINGS, CONTENTDETAIL, VIDEOS) {

        this.enable = function () {
            var mock = basicMock(urlUtil.replaceParams(SETTINGS.resource.contentDetail, '?'), {
                debug: true
            });

            mock.route('GET', '/?', function(request) {
                var params = request.pathArgs;
                var id = params[0];
                angular.forEach(VIDEOS, function(category) {
                    angular.forEach(category.videos, function(video) {
                        if(video.id == id) {
                            CONTENTDETAIL.id = id;
                            CONTENTDETAIL.name = video.name;
                        }
                    })
                });
                return CONTENTDETAIL;
            });

        }

    };

})();