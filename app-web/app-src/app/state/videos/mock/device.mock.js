(function () {
    'use strict'

    angular.module('isamedia-app')
        .service('deviceMock', Mock);

    /* @ngInject */
    function Mock(basicMock, SETTINGS, DEVICES, $interval) {

        var connectedDevice = null;

        var currentTimer = null;

        function startTimer(initialSeconds) {
            if(currentTimer) {
                $interval.cancel(currentTimer);
            }

            if(connectedDevice) {
                connectedDevice.status.currentTime = initialSeconds || 0;
                currentTimer = $interval(function() {
                    if(connectedDevice.status.state = "PLAYING") {
                        connectedDevice.status.currentTime = connectedDevice.status.currentTime + 10;
                    }
                }, 10000);
            }
        }

        this.enable = function () {
            var mock = basicMock(SETTINGS.resource.device, {
                debug: true
            });

            mock.route('GET', '', function(request) {
                return DEVICES;
            });

            mock.route('GET', '/?/connect', function(request) {
                var params = request.pathArgs;
                var deviceName = params[0];

                if(connectedDevice) {
                    connectedDevice.status.connected = false;
                }

                angular.forEach(DEVICES, function(device) {
                    if(device.name == deviceName) {
                        connectedDevice = device;
                    }
                });

                connectedDevice.status.connected = true;

                return connectedDevice;
            });

            mock.route('GET', '/connected', function(request) {
                if(!connectedDevice) {
                    return "";
                }
                return connectedDevice;
            });

            mock.route('POST', '/media/?', function(request) {
                var params = request.pathArgs;
                var mediaId = params[0];
                var language = request.body;

                connectedDevice.status.mediaId = mediaId;
                connectedDevice.status.state = "PLAYING";
                startTimer(language.discardInitialSeconds);
                return "";
            });

            mock.route('GET', '/play', function(request) {
                if(!connectedDevice) {
                    return "";
                }
                connectedDevice.status.state = "PLAYING";
                return connectedDevice;
            });

            mock.route('GET', '/pause', function(request) {
                if(!connectedDevice) {
                    return "";
                }
                connectedDevice.status.state = "PAUSED";
                return connectedDevice;
            });

        }

    };

})();