(function () {
    'use strict'

    angular.module('isamedia-app')
        .service('contentMock', Mock);

    /* @ngInject */
    function Mock(resourceMock, SETTINGS, VIDEOS) {

        this.enable = function () {
            var mock = resourceMock(SETTINGS.resource.contentVideos, {}, {
                debug: true
            });

            var storage = angular.copy(VIDEOS);

            mock.createAction = function (request) {
                storage = request.body;
                return storage;
            }

            mock.indexAction = function (request) {
                return storage;
            }

            mock.getStorage = function (params) {
                return storage;
            }
        }

    };

})();