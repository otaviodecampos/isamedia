(function () {
    'use strict'

    angular.module('isamedia-app')
        .service('videoLanguageMock', Mock);

    /* @ngInject */
    function Mock(basicMock, urlUtil, SETTINGS, VIDEOLANGUAGE) {

        var currentVideoLanguage = {};

        this.enable = function () {
            var mock = basicMock(urlUtil.replaceParams(SETTINGS.resource.videoControl, '?'), {
                debug: true
            });

            mock.route('GET', '/?', function(request) {
                var params = request.pathArgs;
                var mediaId = params[0];
                if(!currentVideoLanguage[mediaId]) {
                    currentVideoLanguage[mediaId] = angular.copy(VIDEOLANGUAGE);
                }
                return currentVideoLanguage[mediaId];
            });

            mock.route('POST', '/?', function(request) {
                var params = request.pathArgs;
                var language = request.body;
                var mediaId = params[0];
                currentVideoLanguage[mediaId] = language;
                return currentVideoLanguage[mediaId];
            });

        }

    };

})();