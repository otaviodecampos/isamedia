(function () {

    angular.module('isamedia-app')
        .service('Device', Service);

    /* @ngInject */
    function Service($http, SETTINGS, $q) {

        var that = this;
        var onLoadFunctions = [];
        var onPlayFunctions = [];
        var onPauseFunctions = [];
        var onConnectFunctions = [];
        that.devices = [];

        that.get = function() {
            return that.devices;
        }

        that.getConnected = function() {
            return $http.get(SETTINGS.resource.deviceConnected);
        }

        that.connect = function(device) {
            var deviceConnectUrl = SETTINGS.resource.deviceConnect;
            deviceConnectUrl = deviceConnectUrl.replace(":name", device.name);
            return $http.get(deviceConnectUrl);
        }

        that.load = function(media, language) {
            var deviceLoadUrl = SETTINGS.resource.deviceLoad;
            deviceLoadUrl = deviceLoadUrl.replace(":id", media.id);
            var request = $http.post(deviceLoadUrl, language);
            request.success(invokeOnLoadFunctions);
            return request;
        }

        that.play = function() {
            var devicePlayUrl = SETTINGS.resource.devicePlay;
            var request = $http.get(devicePlayUrl);
            request.success(invokeOnPlayFunctions);
            return request;
        }

        that.pause = function() {
            var devicePauseUrl = SETTINGS.resource.devicePause;
            var request = $http.get(devicePauseUrl);
            request.success(invokeOnPauseFunctions);
            return request;
        }

        that.onLoad = function(func) {
            onLoadFunctions.push(func);
        }

        that.onPlay = function(func) {
            onPlayFunctions.push(func);
        }

        that.onPause = function(func) {
            onPauseFunctions.push(func);
        }

        that.onConnect = function(func) {
            onConnectFunctions.push(func);
        }

        that.refresh = function() {
            var request = $http.get(SETTINGS.resource.device);
            request.success(function(devices) {
                that.devices = devices;
            });
            return request;
        }
        that.refresh();

        that.getCurrentMediaId = function() {
            var deferred = $q.defer();

            that.getConnected().success(function(device) {
                if(device) {
                    deferred.resolve(device.status.mediaId);
                }
            }).error(function() {
                setShowLoader(false);
                Message.show(MESSAGES.connectedDeviceError);
            });

            return deferred.promise;
        }

        function invokeOnLoadFunctions() {
            angular.forEach(onLoadFunctions, function(loadFunction) {
                loadFunction();
            });
        }

        function invokeOnPlayFunctions() {
            angular.forEach(onPlayFunctions, function(playFunction) {
                playFunction();
            });
        }

        function invokeOnPauseFunctions() {
            angular.forEach(onPauseFunctions, function(pauseFunction) {
                pauseFunction();
            });
        }

        function invokeOnConnectFunctions() {
            angular.forEach(onConnectFunctions, function(connectFunction) {
                connectFunction();
            });
        }

    }

})();