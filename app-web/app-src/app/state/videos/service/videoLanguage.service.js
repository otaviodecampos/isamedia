(function () {

    angular.module('isamedia-app')
        .service('VideoLanguage', Service);

    /* @ngInject */
    function Service($http, SETTINGS) {

        var that = this,
            apiUrl = SETTINGS.resource.videoControl;

        that.get = function(videoId) {
            var url = apiUrl.replace(":id", videoId);
            return $http.get(url);
        }

        that.set = function(videoId, language) {
            var url = apiUrl.replace(":id", videoId);
            return $http.post(url, language);
        }
    }

})();