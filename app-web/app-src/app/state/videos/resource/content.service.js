(function() {

    angular.module('isamedia-app')
        .service('Content', Service);

    /* @ngInject */
    function Service($q, $http, SETTINGS) {
        var that = this;

        var videosUrl = SETTINGS.resource.contentVideos;
        var detailUrl = SETTINGS.resource.contentDetail;

        that.getVideos = function() {
            var deferred = $q.defer();

            $http.get(videosUrl).success(function(videos) {
                deferred.resolve(videos);
            });

            return deferred.promise;
        }

        that.getContentDetail = function(media) {
            return that.getContentDetailById(media.id);
        }

        that.getContentDetailById = function(id) {
            var url = detailUrl.replace(":id", id);
            return $http.get(url);
        }

    }

})();