(function() {

    angular.module('isamedia-app')
        .config(Config);

    function Config(AsadminProvider, $urlRouterProvider, ASADMIN, SETTINGS) {

        $urlRouterProvider.otherwise(SETTINGS.defaultUrl);

        AsadminProvider.setNavigationTitle(ASADMIN.navigationTitle);
        AsadminProvider.setSidebar(ASADMIN.sidebar);

    }

})();