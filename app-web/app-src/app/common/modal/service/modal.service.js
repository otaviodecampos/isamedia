(function () {
    'use strict';

    $(document).ready(function(){
        $('.ui.sticky')
            .sticky({
                context: '.modal'
            });

    })


    angular.module('isamedia-app')
        .service('Modal', Service);

    /* @ngInject */
    function Service($templateRequest, $document, $compile, $rootScope, $q, $controller, $timeout, $parse) {

        var defaultSettings = {
            blurring: true,
            observeChanges: true,
            templateUrl: 'isamedia-app/modal.tpl.html',
            allowMultiple: true
        }

        this.open = function (userSettings) {
            var settings = angular.extend({}, defaultSettings, userSettings);
            return _open(settings);
        }

        function _open(settings) {
            var deferred = $q.defer()
                , templateUrl
                , scope;

            templateUrl = angular.isFunction(settings.templateUrl) ?
                settings.templateUrl() : settings.templateUrl;

            scope = settings.scope ?
                settings.scope.$new() : $rootScope.$new();

            $q.when($templateRequest(templateUrl)).then(function (template) {
                var element = $compile(template)(scope)
                    , target = $document.find('body').eq(0)
                    , approve;

                if (settings.controller) {
                    var resolve = {$scope: scope};

                    if(settings.resolve) {
                        angular.extend(resolve, settings.resolve);
                    }

                    resolve.closeModal = function() {
                        element.modal('hide');
                    }

                    $controller(settings.controller, resolve);
                }

                target.append(element);

                scope.$on('$destroy', function() {
                    element.remove();
                });

                element.modal({
                    allowMultiple: settings.allowMultiple,
                    blurring: settings.blurring,
                    observeChanges: settings.observeChanges,
                    onHide: function () {
                        if (approve) {
                            var resolveResult;
                            if(settings.resolveResult) {
                                if(angular.isFunction(settings.resolveResult)) {
                                    resolveResult = settings.resolveResult();
                                } else {
                                    resolveResult = $parse(settings.resolveResult)(scope);
                                }
                            }
                            deferred.resolve(resolveResult);
                        } else {
                            deferred.reject();
                        }
                    },
                    onApprove: function () {
                        approve = true;
                    },
                    onHidden: function() {
                        scope.$destroy();
                    },
                    onShow: function() {
                        if(settings.onShow) {
                            settings.onShow.call(element);
                        }
                    }
                });

                element.click(function() {
                    $timeout(function() {
                        element.modal('refresh');
                    });
                });

                element.modal('show');
            });

            return deferred.promise;
        }
    }

})();