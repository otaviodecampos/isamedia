(function() {
    'use strict'

    angular.module('isamedia-app')
        .service('urlUtil', Service);

    /* @ngInject */
    function Service() {

        this.replaceParams = function (url, replace) {
            url = url.replace(/\/:\w+/g, replace || '/');

            if(replace) {
                url = url.substring(0, url.lastIndexOf(replace));
            }

            return url;
        }

    };

})();