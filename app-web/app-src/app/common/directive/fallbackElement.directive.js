(function () {

    angular.module('isamedia-app')
        .directive('fallbackElement', Directive);

    function Directive($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var targetElement = element.siblings(attrs.fallbackElement);
                targetElement.hide();
                element.on('error', function () {
                    angular.element(this).hide();
                    targetElement.show();
                });
            }
        };
    }

})();