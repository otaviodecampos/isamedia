(function () {

    angular.module('isamedia-app')
        .config(Config);

    /* @ngInject */
    function Config($provide, SETTINGS) {

        var delay = SETTINGS.mock.delay || 0;

        $provide.decorator('$httpBackend', function ($delegate) {
            for (var key in $delegate) {
                proxy[key] = $delegate[key];
            }

            function proxy(method, url, data, callback, headers) {
                var interceptor = function () {
                    var that = this
                        , args = arguments;

                    setTimeout(function () {
                        callback.apply(that, args);
                    }, delay);
                }
                return $delegate.call(this, method, url, data, interceptor, headers);
            }

            return proxy;
        });

    }


})();