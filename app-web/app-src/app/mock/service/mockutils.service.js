(function() {
    'use strict'

    angular.module('isamedia-app')
        .service('mockUtils', Service);

    /* @ngInject */
    function Service(urlUtil) {

        this.array = function (array) {
            var mock = {};

            angular.forEach(array, function(item, i) {
                var id = item.id != undefined ? item.id : i;
                mock[id] = item;
                item.id = id;
            });

            return mock;
        }

        this.url = function (url) {
            return urlUtil.replaceParams(url, '/?');
        }

    };

})();