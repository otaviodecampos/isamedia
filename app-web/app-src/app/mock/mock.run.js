(function () {
    'use strict'

    angular.module('isamedia-app')
        .run(Run);

    /* @ngInject */
    function Run($httpBackend, contentMock, deviceMock, contentDetailMock, videoLanguageMock, SETTINGS) {

        $httpBackend.whenGET(/^\/assets/).passThrough();

        if (SETTINGS.mock.enabled == "true") {
            contentMock.enable();
            deviceMock.enable();
            contentDetailMock.enable();
            videoLanguageMock.enable();
        } else {
            $httpBackend.whenGET(/[\s\S]*/).passThrough();
            $httpBackend.whenPOST(/[\s\S]*/).passThrough();
        }

    };

})();