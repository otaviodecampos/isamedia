(function () {

    angular.module('isamedia-app')
        .service('Message', Service);

    /* @ngInject */
    function Service($rootScope, Modal) {

        var that = this;

        var modalOptions = {
            templateUrl: 'isamedia-app/message.tpl.html'
        };

        that.show = function(text) {
            modalOptions.scope = $rootScope.$new();
            modalOptions.scope.text = text;
            Modal.open(modalOptions);
        }

    }

})();