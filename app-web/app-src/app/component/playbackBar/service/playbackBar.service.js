(function () {

    angular.module('isamedia-app')
        .service('PlaybackBar', Service);

    /* @ngInject */
    function Service(Device, Content, Message) {
        var that = this;
        that.currentMedia = null;

        that.initialize = initialize;
        that.initialize();

        Device.onLoad(initialize);

        function initialize() {
            Device.getConnected().success(function(device) {
                if(device && device.status.mediaId != undefined) {
                    Content.getContentDetailById(device.status.mediaId).success(function(media) {
                        that.currentMedia = media;
                    }).error(function() {
                        Message.show(MESSAGES.getContentDetailError);
                    });
                }
            });
        }

    }

})();