(function () {

    angular.module('isamedia-app')
        .directive('playbackBar', Directive);

    function Directive() {
        return {
            restrict: 'E',
            controller: 'PlaybackController as playback',
            templateUrl: 'isamedia-app/playbackBar.tpl.html',
            replace: true,
            link: function (scope, element, attrs, ctrl, transcludeFn) {

            }
        };
    }

})();