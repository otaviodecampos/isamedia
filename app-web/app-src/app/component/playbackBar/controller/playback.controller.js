(function () {

    angular.module('isamedia-app')
        .controller('PlaybackController', Controller);

    /* @ngInject */
    function Controller(PlaybackBar, Device) {

        var that = this;
        that.service = PlaybackBar;
        that.currentIconState = null;

        that.play = function() {
            Device.play().success(setPlaybackCurrentIconState);
        }

        that.pause = function() {
            Device.pause().success(setPlaybackCurrentIconState);
        }

        that.openCurrent = function() {

        }

        Device.onLoad(setPlaybackCurrentIconState);
        Device.onPlay(setPlaybackCurrentIconState);
        Device.onPause(setPlaybackCurrentIconState);

        setPlaybackCurrentIconState();
        function setPlaybackCurrentIconState() {
            Device.getConnected().success(function(device) {
                if(device) {
                    if(device.status.state == "PLAYING" || device.status.state == "BUFFERING") {
                        that.currentIconState = "PAUSE"
                    } else if(device.status.state == "PAUSED") {
                        that.currentIconState = "PLAY"
                    }
                }
            });
        }

    }

})();