var gulp = require('gulp')
    , concat = require('gulp-concat')
    , ngjson = require('gulp-ng-json')
    , templatecache = require('gulp-angular-templatecache')
    , es = require('event-stream')
    , order = require("gulp-order")
    , ngannotate = require('gulp-ng-annotate')
    , settings = require('./settings.json').compile
    , replace = require('gulp-replace')
    , isProduction = require('yargs').argv.production == true
    , properties = require('../../properties.json')
    , debug = require('./debug');

module.exports = function () {

    var env = isProduction && 'production' || 'develop';
    debug.context('Compiling with', debug.blue(env), 'properties..');

    var that = this;

    var appInput = that.input(that.srcDir, ['**/*.json', '**/*.js']);
    var appSrc = gulp.src(appInput)
        .pipe(ngjson.module())
        .pipe(ngjson.constant())
        .pipe(ngjson.state());

    var templateInput = that.input(that.srcDir, ['**/*.tpl.html']);
    var templateSrc = gulp.src(templateInput)
        .pipe(templatecache({
            module: that.buildName,
            transformUrl: function (url) {
                return that.buildName + '/' + url.match(/[\w-.]+.tpl.html$/g)[0];
            }
        }));

    var mergeSrc = gulp.src(settings.files);

    return es.merge(appSrc, templateSrc, mergeSrc)
        .pipe(order(settings.order))
        .pipe(ngannotate(settings.ngAnnotate))
        .pipe(replace(/\$\{[\w\.]+\}/g, function(prop) {
            prop = prop.replace(/^\${|}$/g, '');
            return properties[env][prop];
        }))
        .pipe(concat(that.buildName + '.js'))
        .pipe(gulp.dest(that.buildDir + settings.targetDir));

}