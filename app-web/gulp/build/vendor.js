var gulp = require('gulp')
    , concat = require('gulp-concat')
    , settings = require('./settings.json').vendor;

module.exports = function () {

    var that = this;

    var filesSrc = gulp.src(settings.files)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(that.buildDir + settings.targetDir));

}