var gulp = require('gulp')
    , less = require('gulp-less')
    , concat = require('gulp-concat')
    , settings = require('./settings.json').css;

module.exports = function () {

    var that = this;

    var cssInput = this.input(that.srcDir, ['css/' + that.buildName + '.less']);

    return gulp.src(cssInput)
        .pipe(less())
        .pipe(concat(that.buildName + '.css'))
        .pipe(gulp.dest(that.buildDir + settings.targetDir));

}