var gulp = require('gulp')
    , filterFiles = require('../filter/files')
    , settings = require('./settings.json').copy;

module.exports = function build() {

    var that = this;

    var copyInput = that.input(that.srcDir, [
        '**/*',
        '!app/**/*',
        '!css/**/*'
    ]);

    return gulp.src(copyInput)
        .pipe(filterFiles())
        .pipe(gulp.dest(that.buildDir));

}