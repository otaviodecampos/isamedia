var gulp = require('gulp')
    , filterFiles = require('../filter/files')
    , settings = require('./settings.json').fonts;

module.exports = function build() {

    var that = this;

    return gulp.src(settings.files)
        .pipe(filterFiles())
        .pipe(gulp.dest(that.buildDir + settings.targetDir));

}