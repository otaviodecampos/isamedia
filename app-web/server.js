var connect = require('connect');
var serveStatic = require('serve-static');
var path = __dirname + '/app-dist';
var ip = process.env.IP || '192.168.1.127'
var port = process.env.PORT || 8080;

connect().use('/', serveStatic(path)).listen(port, ip, function() {
    console.log('Web Server running on ' + ip + ':' + port + '...');
});