package com.isamedia.dlnaserver.api.contentdirectory.folder.converter;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.converter.ConvertException;
import org.fourthline.cling.support.model.DIDLObject;

/**
 * Created by otavio on 26/08/2016.
 */
public interface Converter<Source extends Content, Target extends DIDLObject> {

    Target convert(Source source) throws ConvertException;

}
