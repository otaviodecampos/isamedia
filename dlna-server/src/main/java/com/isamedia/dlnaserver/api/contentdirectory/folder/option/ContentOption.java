package com.isamedia.dlnaserver.api.contentdirectory.folder.option;

import javax.enterprise.util.Nonbinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by otavio on 17/09/2016.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContentOption {

    @Nonbinding
    Class<? extends ContentOptionBuilder>[] value();

}
