package com.isamedia.dlnaserver.api.contentdirectory.folder;

/**
 * Created by otavio on 10/09/2016.
 */
public interface MediaDetailConstant {

    String MEDIA_ADDRESS = "%s/api/media/%s";

    String MEDIA_TRANSCODE_ADDRESS = "%s/api/media/%s/transcode";

    String MEDIA_THUMBNAIL_ADDRESS = "%s/api/media/%s/screen/100";

    String VIDEO_ADDRESS_PARAMS = "?audioStream=%d&subtitleStream=%d&externalSubtitleIndex=%d&discardInitialTime=%d";

}
