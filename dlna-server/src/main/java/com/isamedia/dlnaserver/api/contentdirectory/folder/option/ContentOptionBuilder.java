package com.isamedia.dlnaserver.api.contentdirectory.folder.option;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;

import java.util.List;

/**
 * Created by otavio on 17/09/2016.
 */
public interface ContentOptionBuilder<T extends Content> {

    boolean isValid(T content);

    List<Content> build(T content);

}
