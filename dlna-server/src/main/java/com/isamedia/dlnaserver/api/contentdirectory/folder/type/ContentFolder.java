package com.isamedia.dlnaserver.api.contentdirectory.folder.type;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;

/**
 * Created by otavio on 06/10/2016.
 */
@Qualifier
@Target({TYPE, FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ContentFolder {
}
