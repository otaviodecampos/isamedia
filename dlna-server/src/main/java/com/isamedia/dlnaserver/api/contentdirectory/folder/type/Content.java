package com.isamedia.dlnaserver.api.contentdirectory.folder.type;

import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;

/**
 * Created by otavio on 25/08/2016.
 */
public interface Content {

    Integer getId();

    void setId(Integer id);

    String getName();

    void setName(String name);

    Folder getParent();

    void setParent(Folder folder);

    String getPath();

    void setPath(String path);

}
