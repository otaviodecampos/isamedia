package com.isamedia.dlnaserver.api;

/**
 * Created by otavio on 17/09/2016.
 */
public interface Notifier {

    void notify(Exception e);

}
