package com.isamedia.dlnaserver.api;

/**
 * Created by otavio on 18/09/2016.
 */
public interface LogMessage {

    String DATABASE_ANALYSIS_FINISHED = "Database analysis finished.";

    String DATABASE_SCAN_FINISHED = "Database scan finished.";

}
