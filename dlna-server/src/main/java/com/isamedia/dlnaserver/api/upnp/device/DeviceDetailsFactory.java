package com.isamedia.dlnaserver.api.upnp.device;

import com.isamedia.dlnaserver.impl.upnp.UpnpConstant;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.ManufacturerDetails;
import org.fourthline.cling.model.meta.ModelDetails;
import org.fourthline.cling.model.profile.HeaderDeviceDetailsProvider;
import org.fourthline.cling.model.types.DLNACaps;
import org.fourthline.cling.model.types.DLNADoc;

import java.util.List;

/**
 * Created by otavio on 24/08/2016.
 */
public interface DeviceDetailsFactory {

    String getModelName();

    String getModelDescription();

    String getModelNumber();

    List<HeaderDeviceDetailsProvider.Key> getHeaderDeviceDetailsProviderKeys();

    default DeviceDetails createDeviceDetails() {
        return new DeviceDetails(
                UpnpConstant.MEDIA_SERVER_NAME,
                new ManufacturerDetails(UpnpConstant.MEDIA_SERVER_NAME),
                new ModelDetails(getModelName(), getModelDescription(), getModelNumber()),
                UpnpConstant.DEVICE_SERIAL_NUMBER,
                UpnpConstant.DEVICE_UPC,
                UpnpConstant.DEVICE_PRESENTATION_URL,
                new DLNADoc[]{
                        new DLNADoc("DMS", DLNADoc.Version.V1_5),
                },
                new DLNACaps(new String[]{
                        "av-upload", "image-upload", "audio-upload"
                })
        );
    }

}
