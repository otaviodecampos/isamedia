package com.isamedia.dlnaserver.impl.content.folder;

import com.isamedia.common.impl.Settings;
import com.isamedia.dlnaserver.impl.ServerDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.vo.VideoControl;

import javax.inject.Inject;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static com.isamedia.common.api.MediaTranscode.EVER;
import static com.isamedia.dlnaserver.api.contentdirectory.folder.MediaDetailConstant.*;

/**
 * Created by otavio on 27/08/2016.
 */
public class MediaDetail {

    @Inject
    private ServerDetail serverDetail;

    @Inject
    private Settings settings;

    public String getHttpAddress(Media media) {
        boolean transcode = EVER.equals(settings.getMediaTranscode()) || media.isDuplicated();
        String addressTemplate = transcode ? MEDIA_TRANSCODE_ADDRESS : MEDIA_ADDRESS;
        return String.format(addressTemplate, serverDetail.getHttpServerAddress(), media.getId());
    }

    public String getVideoHttpAddress(Media media, VideoControl videoControl) {
        String url = getHttpAddress(media);
        url += String.format(VIDEO_ADDRESS_PARAMS, videoControl.getSelectedAudio().getStream(), videoControl.getSelectedSubtitle().getStream(), videoControl.getSelectedSubtitle().getExternalIndex(), videoControl.getDiscardInitialSeconds());
        return url;
    }

    public String getThumbnailHttpAddress(Media media) {
        String addressTemplate = MEDIA_THUMBNAIL_ADDRESS;
        return String.format(addressTemplate, serverDetail.getHttpServerAddress(), media.getId());
    }

    public LocalTime getDurationTime(Media media) {
        LocalTime durationTime = null;
        try{
            durationTime = LocalTime.parse(media.analyse().getFormat().getDuration(), DateTimeFormatter.ofPattern("H:mm:ss[.SSSSSS]"));
        } catch(Exception e) {
            durationTime = LocalTime.parse("0:00:00.000000", DateTimeFormatter.ofPattern("H:mm:ss[.SSSSSS]"));
        }
        return durationTime;
    }

    public int getDurationInSeconds(Media media) {
        return getDurationTime(media).toSecondOfDay();
    }

}
