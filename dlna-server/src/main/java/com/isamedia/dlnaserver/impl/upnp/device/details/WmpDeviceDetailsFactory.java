package com.isamedia.dlnaserver.impl.upnp.device.details;

import com.isamedia.dlnaserver.api.upnp.device.DeviceDetailsFactory;
import org.fourthline.cling.model.profile.HeaderDeviceDetailsProvider;

import java.util.*;

/**
 * Created by otavio on 24/08/2016.
 */
public class WmpDeviceDetailsFactory implements DeviceDetailsFactory {

    @Override
    public String getModelName() {
        return "Windows Media Player Sharing";
    }

    @Override
    public String getModelDescription() {
        return "Windows Media Player Sharing";
    }

    @Override
    public String getModelNumber() {
        return "12.0";
    }

    @Override
    public List<HeaderDeviceDetailsProvider.Key> getHeaderDeviceDetailsProviderKeys() {
        return Arrays.asList(new HeaderDeviceDetailsProvider.Key("User-Agent", "FDSSDP"), new HeaderDeviceDetailsProvider.Key("User-Agent", "Xbox.*"));
    }

}
