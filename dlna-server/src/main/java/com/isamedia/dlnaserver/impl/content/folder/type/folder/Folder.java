package com.isamedia.dlnaserver.impl.content.folder.type.folder;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.GroupFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.serializer.FolderSerializer;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by otavio on 25/08/2016.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(using = FolderSerializer.class)
public class Folder implements Content {

    private final List<Content> children = new ArrayList<>();

    private Integer id;

    @NonNull
    private String name;

    private String path;

    private Folder parent;

    public List<Folder> getChildrenFolder() {
        return getChildren().stream().filter(item -> item instanceof Folder).map(folder -> (Folder) folder).collect(Collectors.toList());
    }

    public List<Media> getChildrenMedia() {
        return getChildren().stream().filter(item -> item instanceof Media && !((Media) item).isInvalid()).map(folder -> (Media) folder).collect(Collectors.toList());
    }

    public List<Video> getChildrenVideo() {
        return getChildren().stream().filter(item -> item instanceof Video && !((Video) item).isInvalid()).map(folder -> (Video) folder).collect(Collectors.toList());
    }

    public List<VirtualFolder> getChildrenVirtual() {
        return getChildren().stream().filter(item -> item instanceof VirtualFolder).map(folder -> (VirtualFolder) folder).collect(Collectors.toList());
    }

    public List<GroupFolder> getChildrenGroup() {
        return getChildren().stream().filter(item -> item instanceof GroupFolder).map(folder -> (GroupFolder) folder).collect(Collectors.toList());
    }

    public Folder getCommonParent() {
        return getCommonParent(false);
    }

    public Folder getCommonParent(boolean includeGroupFolder) {
        Folder parent = getParent();
        if (parent != null) {
            boolean isGroupFolder = !includeGroupFolder && parent.getChildrenGroup().size() == parent.getChildren().size();
            if (isGroupFolder || !parent.getChildrenMedia().isEmpty() || parent.getChildrenFolder().size() == 1) {
                Folder nextParent = parent.getCommonParent(includeGroupFolder);
                if (nextParent != null) {
                    parent = nextParent;
                }
            }
        }
        return parent;
    }

    public <T extends Content> void addChildren(List<T> items) {
        addChildren(items.stream().toArray(Content[]::new));
    }

    public <T extends Content> void addChildren(T... items) {
        for (Content content : items) {
            content.setParent(this);
            children.add(content);
        }
    }

    public <T extends Content> void removeChildren(List<T> items) {
        removeChildren(items.stream().toArray(Content[]::new));
    }

    public <T extends Content> void removeChildren(T... items) {
        for (Content content : items) {
            content.setParent(null);
            children.remove(children.indexOf(content));
        }
    }

}
