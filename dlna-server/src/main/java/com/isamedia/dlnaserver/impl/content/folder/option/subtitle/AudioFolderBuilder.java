package com.isamedia.dlnaserver.impl.content.folder.option.subtitle;

import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.option.OptionHelper;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.GroupFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;
import com.isamedia.media.impl.analysis.type.MediaStream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 17/09/2016.
 */
public class AudioFolderBuilder implements ContentOptionBuilder<SubtitleFolder> {

    @Override
    public boolean isValid(SubtitleFolder subtitleFolder) {
        return subtitleFolder.getOwner().analyse().getAudioStreams().size() > 1 && !(subtitleFolder.getParent() instanceof VirtualFolder) || subtitleFolder.getParent() instanceof GroupFolder;
    }

    @Override
    public List<Content> build(SubtitleFolder subtitleFolder) {
        List<Content> contents = new ArrayList<>();

        List<Folder> languageFolders = subtitleFolder.getChildrenFolder();
        for (Folder languageFolder : languageFolders) {
            VirtualFolder folder = (VirtualFolder) languageFolder;
            Media owner = folder.getOwner();

            AudioFolder audioFolder = new AudioFolder();
            audioFolder.setOwner(owner);
            folder.addChildren(audioFolder);
            contents.add(audioFolder);

            for (MediaStream stream : owner.analyse().getAudioStreams()) {
                contents.addAll(OptionHelper.buildAudioStreamOption(audioFolder, (Video) owner, stream));
            }
        }

        return contents;
    }

}
