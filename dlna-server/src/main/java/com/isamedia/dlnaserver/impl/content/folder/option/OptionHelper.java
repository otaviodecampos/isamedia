package com.isamedia.dlnaserver.impl.content.folder.option;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoExternalSubtitle;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;
import com.isamedia.common.impl.util.StringUtil;
import com.isamedia.media.impl.analysis.type.MediaStream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 17/09/2016.
 */
public class OptionHelper {

    public static List<Content> buildAudioStreamOption(Folder parent, Video video, MediaStream stream) {
        List<Content> contents = new ArrayList<>();

        VirtualFolder streamFolder = new VirtualFolder();
        streamFolder.setName(StringUtil.capitalize(stream.getLanguage()));
        parent.addChildren(streamFolder);
        contents.add(streamFolder);

        Video videoOption = video.duplicate();
        streamFolder.setOwner(videoOption);
        streamFolder.addChildren(videoOption);
        videoOption.getParams().setAudioStream(video.analyse().getAudioStreams().indexOf(stream));
        contents.add(videoOption);

        return contents;
    }

    public static List<Content> buildSubtitleStreamOption(Folder parent, Video video, MediaStream stream) {
        List<Content> contents = new ArrayList<>();

        VirtualFolder streamFolder = new VirtualFolder();
        streamFolder.setName(StringUtil.capitalize(stream.getLanguage()));
        parent.addChildren(streamFolder);
        contents.add(streamFolder);

        Video videoOption = video.duplicate();
        streamFolder.setOwner(videoOption);
        streamFolder.addChildren(videoOption);
        videoOption.getParams().setSubtitleStream(video.analyse().getSubtitleStreams().indexOf(stream));
        videoOption.getParams().setSubtitleGraphic(stream.isSubtitleGraphic());
        contents.add(videoOption);

        return contents;
    }

    public static List<Content> buildSubtitleExternalOption(Folder parent, Video video, VideoExternalSubtitle externalSubtitle) {
        List<Content> contents = new ArrayList<>();

        VirtualFolder streamFolder = new VirtualFolder();
        streamFolder.setName(StringUtil.capitalize(externalSubtitle.getName()));
        parent.addChildren(streamFolder);
        contents.add(streamFolder);

        Video videoOption = video.duplicate();
        streamFolder.setOwner(videoOption);
        streamFolder.addChildren(videoOption);
        videoOption.getParams().setExternalSubtitle(externalSubtitle.getPath());
        contents.add(videoOption);

        return contents;
    }

}
