package com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual;

import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.common.impl.util.FilenameUtil;
import lombok.Data;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
public class GroupFolder extends VirtualFolder {

    public GroupFolder(Media owner) {
        setName(FilenameUtil.removeExtension(owner.getName()));
        setOwner(owner);
    }

}
