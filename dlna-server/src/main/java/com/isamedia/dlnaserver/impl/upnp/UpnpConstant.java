package com.isamedia.dlnaserver.impl.upnp;

/**
 * Created by otavio on 24/08/2016.
 */
public class UpnpConstant {

    public static final String MEDIA_SERVER_NAME = "Isamedia";

    public static final String DEVICE_SERIAL_NUMBER = "000da201238c";

    public static final String DEVICE_UPC = "100000000001";

    public static final String DEVICE_PRESENTATION_URL = "http://www.isamediaserver.org/some_user_interface/";

    public static final String DEVICE_TYPE = "MediaServer";

}
