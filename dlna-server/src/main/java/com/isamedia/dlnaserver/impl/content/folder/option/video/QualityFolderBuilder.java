package com.isamedia.dlnaserver.impl.content.folder.option.video;

import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.QualityFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoQuality;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 18/09/2016.
 */
@NoArgsConstructor
public class QualityFolderBuilder implements ContentOptionBuilder<Video> {

    private final int HEIGHT_TOLERANCE = 100;

    @Override
    public boolean isValid(Video video) {
        return !video.isDuplicated() || (video.getParent() != null && (video.getParent().getParent() instanceof AudioFolder || video.getParent().getParent() instanceof SubtitleFolder) && !hasQualityFolder(video.getParent()));
    }

    @Override
    public List<Content> build(Video video) {
        List<Content> contents = new ArrayList<>();

        QualityFolder folder = new QualityFolder();
        video.getParent().addChildren(folder);
        folder.setOwner(video);
        contents.add(folder);

        for (VideoQuality quality : VideoQuality.values()) {
            if (quality == VideoQuality.NONE || video.analyse().getVideoStream().getHeight() >= quality.getHeight() - HEIGHT_TOLERANCE) {
                VirtualFolder qualityFolder = new VirtualFolder();
                folder.addChildren(qualityFolder);
                qualityFolder.setName(quality.getLabel());

                Video qualityOption = video.duplicate();
                qualityOption.getParams().setQuality(quality);
                qualityFolder.setOwner(qualityOption);
                qualityFolder.addChildren(qualityOption);

                contents.add(qualityOption);
                contents.add(qualityFolder);
            }
        }

        return contents;
    }

    private boolean hasQualityFolder(Folder folder) {
        return folder.getChildrenVirtual().stream().filter(children -> children instanceof QualityFolder).limit(1).findFirst().isPresent();
    }

}
