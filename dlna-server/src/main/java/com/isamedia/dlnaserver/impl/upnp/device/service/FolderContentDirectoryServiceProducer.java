package com.isamedia.dlnaserver.impl.upnp.device.service;

import com.isamedia.dlnaserver.impl.content.folder.FolderContentDirectory;
import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.meta.LocalService;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Created by otavio on 24/08/2016.
 */
public class FolderContentDirectoryServiceProducer {

    @Inject
    private FolderContentDirectory folderContentDirectory;

    @Produces
    public LocalService create() {
        LocalService<FolderContentDirectory> contentDirectory = new AnnotationLocalServiceBinder().read(FolderContentDirectory.class);
        contentDirectory.setManager(createContentDirectoryServiceManager(contentDirectory));
        return contentDirectory;
    }

    private DefaultServiceManager<FolderContentDirectory> createContentDirectoryServiceManager(LocalService<FolderContentDirectory> directoryManagerService) {
        return new DefaultServiceManager<FolderContentDirectory>(directoryManagerService) {
            @Override
            protected FolderContentDirectory createServiceInstance() throws Exception {
                return folderContentDirectory;
            }
        };
    }

}
