package com.isamedia.dlnaserver.impl.content.folder.option.video;

import com.isamedia.common.impl.i18n.I18nTranslator;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.option.OptionHelper;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoExternalSubtitle;
import com.isamedia.media.impl.analysis.type.MediaStream;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.isamedia.dlnaserver.impl.content.folder.option.OptionHelper.buildSubtitleExternalOption;

/**
 * Created by otavio on 17/09/2016.
 */
public class SubtitleFolderBuilder implements ContentOptionBuilder<Video> {

    @Inject
    private I18nTranslator translator;

    @Override
    public boolean isValid(Video video) {
        return !video.isDuplicated() && (video.analyse().getSubtitleStreams().size() > 0 || video.getExternalSubtitles().size() > 0);
    }

    @Override
    public List<Content> build(Video video) {
        SubtitleFolder subtitleFolder = new SubtitleFolder();
        subtitleFolder.setOwner(video);

        List<Content> contents = new ArrayList<>();
        contents.add(subtitleFolder);

        Folder parent = video.getParent();
        parent.addChildren(subtitleFolder);

        for (MediaStream stream : video.analyse().getSubtitleStreams()) {
            contents.addAll(OptionHelper.buildSubtitleStreamOption(subtitleFolder, video, stream));
        }

        for(VideoExternalSubtitle externalSubtitle : video.getExternalSubtitles()) {
            contents.addAll(buildSubtitleExternalOption(subtitleFolder, video, externalSubtitle));
        }

        return contents;
    }

}
