package com.isamedia.dlnaserver.impl.content.folder.type;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.serializer.MediaSerializer;
import com.isamedia.media.impl.analysis.MediaAnalysisManager;
import com.isamedia.media.impl.analysis.type.MediaAnalysis;
import lombok.*;

/**
 * Created by otavio on 25/08/2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(using = MediaSerializer.class)
public abstract class Media implements Content, Cloneable {

    private Integer id;

    @NonNull
    private String name;

    private Folder parent;

    private boolean duplicated;

    @NonNull
    private String path;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private MediaAnalysis analysis;

    private MediaAnalysisManager analysisManager = ApplicationManager.getBean(MediaAnalysisManager.class);

    @NonNull
    private String mimeType;

    @Getter
    @Setter
    private boolean invalid;

    public abstract String getType();

    public MediaAnalysis analyse() {
        if (analysis == null) {
            analysis = analysisManager.get(getPath());
            if (analysis.getStreams().size() == 0) {
                invalid = true;
            }
        }
        return analysis;
    }

    public <T extends Media> T duplicate() {
        try {
            T clone = (T) clone();
            clone.setDuplicated(true);
            clone.setId(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return (T) this;
        }
    }

}
