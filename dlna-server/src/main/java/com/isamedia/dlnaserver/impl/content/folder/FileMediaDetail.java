package com.isamedia.dlnaserver.impl.content.folder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by otavio on 27/08/2016.
 */
public class FileMediaDetail {

    public static boolean isMediaFile(File file) {
        return isVideoFile(file) || isAudioFile(file);
    }

    public static boolean isVideoMimeType(String mimeType) {
        return mimeType.startsWith("video");
    }

    public static boolean isAudioMimeType(String mimeType) {
        return mimeType.startsWith("audio");
    }

    public static boolean isVideoFile(String filePath) {
        return isVideoFile(new File(filePath));
    }

    public static boolean isVideoFile(File file) {
        return isVideoMimeType(getMimeType(file));
    }

    public static boolean isAudioFile(String filePath) {
        return isAudioFile(new File(filePath));
    }

    public static boolean isAudioFile(File file) {
        return isAudioMimeType(getMimeType(file));
    }

    public static String getMimeType(String filePath) {
        return getMimeType(new File(filePath));
    }

    public static String getMimeType(File file) {
        String mimeType = "";
        try {
            String type = Files.probeContentType(file.toPath());
            if (type != null) {
                mimeType = type;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return mimeType;
    }

}
