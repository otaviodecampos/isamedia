package com.isamedia.dlnaserver.impl.content.converter;

import com.isamedia.dlnaserver.api.contentdirectory.folder.converter.Converter;
import com.isamedia.dlnaserver.impl.content.folder.MediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.audio.MediaAudio;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.media.impl.analysis.type.MediaAnalysis;
import org.fourthline.cling.support.model.Res;
import org.fourthline.cling.support.model.item.AudioItem;
import org.fourthline.cling.support.model.item.Item;
import org.fourthline.cling.support.model.item.VideoItem;
import org.seamless.util.MimeType;

import javax.inject.Inject;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 25/08/2016.
 */
public class MediaConverter implements Converter<Media, Item> {

    @Inject
    private MediaDetail mediaDetail;

    @Override
    public Item convert(Media media) {
        Item item = null;
        if (media instanceof Video) {
            item = convertToVideo(media);
        } else if (media instanceof MediaAudio) {
            item = convertToAudio(media);
        }
        return item;
    }

    private VideoItem convertToVideo(Media media) {
        Video video = (Video) media;
        VideoItem videoItem = null;

        try {
            videoItem = convertToItem(media, VideoItem.class);
        } catch (ConvertException e) {
            e.printStackTrace();
        }

        MediaAnalysis analysis = media.analyse();
        Res res = new Res(MimeType.valueOf(media.getMimeType()), null, mediaDetail.getHttpAddress(media));
        res.setBitrate(analysis.getFormat().getBitRate());
        res.setResolution(analysis.getVideoStream().getWidth(), analysis.getVideoStream().getHeight());
        res.setNrAudioChannels(Long.valueOf(analysis.getAudioStreams().size()));

        String duration = analysis.getFormat().getDuration();
        if (video.getParams().getDiscardInitialSeconds() != null) {
            LocalTime maxTime = mediaDetail.getDurationTime(video);
            maxTime = maxTime.minusSeconds(video.getParams().getDiscardInitialSeconds());
            duration = maxTime.format(DateTimeFormatter.ofPattern("H:mm:ss[.SSSSSS]"));
        }
        res.setDuration(duration);

        Res res2 = new Res(MimeType.valueOf("image/jpeg"), null, mediaDetail.getThumbnailHttpAddress(media));

        List<Res> resources = new ArrayList();
        resources.add(res);
        resources.add(res2);
        videoItem.setResources(resources);

        return videoItem;
    }

    private AudioItem convertToAudio(Media media) {
        AudioItem audioItem = null;

        try {
            audioItem = convertToItem(media, AudioItem.class);
        } catch (ConvertException e) {
            e.printStackTrace();
        }

        return audioItem;
    }

    private <T extends Item> T convertToItem(Media media, Class<T> itemClass) throws ConvertException {
        T item;

        try {
            item = itemClass.newInstance();
        } catch (Exception e) {
            throw new ConvertException(e);
        }

        item.setId(String.valueOf(media.getId()));
        item.setParentID(String.valueOf(media.getParent().getId()));
        item.setTitle(media.getName());
        item.setRestricted(true);

        return item;
    }

}
