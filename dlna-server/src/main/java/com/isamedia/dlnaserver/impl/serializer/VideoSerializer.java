package com.isamedia.dlnaserver.impl.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.common.impl.util.StringUtil;
import com.isamedia.dlnaserver.impl.content.folder.MediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by otavio on 22/10/2016.
 */
public class VideoSerializer extends StdSerializer<Video> {

    public VideoSerializer() {
        this(null);
    }

    public VideoSerializer(Class<Video> t) {
        super(t);
    }

    @Override
    public void serialize(Video video, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", video.getId());
        jsonGenerator.writeStringField("name", video.getName());
        jsonGenerator.writeStringField("mimeType", video.getMimeType());
        jsonGenerator.writeStringField("type", video.getType());

        String duration = video.analyse().getFormat().getDuration();
        if (duration != null) {
            duration = duration.substring(0, duration.lastIndexOf("."));
            jsonGenerator.writeStringField("duration", duration);

            MediaDetail mediaDetail = ApplicationManager.getBean(MediaDetail.class);
            jsonGenerator.writeNumberField("durationInSeconds", mediaDetail.getDurationInSeconds(video));
        }

        jsonGenerator.writeStringField("audio", String.join(" / ", video.analyse().getAudioStreams().stream().map(audioStream -> StringUtil.capitalize(audioStream.getLanguage())).collect(Collectors.toList())));

        List<String> subtitles = video.analyse().getSubtitleStreams().stream().map(subtitleStream -> StringUtil.capitalize(subtitleStream.getLanguage())).collect(Collectors.toList());
        if (!video.getExternalSubtitles().isEmpty()) {
            subtitles.add("External");
        }
        jsonGenerator.writeStringField("subtitle", String.join(" / ", subtitles));

        jsonGenerator.writeEndObject();
    }
}
