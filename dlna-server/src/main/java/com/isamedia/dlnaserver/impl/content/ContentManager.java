package com.isamedia.dlnaserver.impl.content;

import com.isamedia.common.api.Startup;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.ContentFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by otavio on 25/08/2016.
 */
@Default
@Startup
@ApplicationScoped
public class ContentManager {

    int idSequence = 100;

    @Inject
    @ContentFolder
    private Instance<Folder> folders;

    private Map<Integer, Content> contents = new ConcurrentHashMap<>();

    public Collection<Content> get() {
        return contents.values();
    }

    public <T extends Content> T get(String id) {
        return get(Integer.valueOf(id));
    }

    public <T extends Content> T get(int id) {
        return (T) contents.get(id);
    }

    @PostConstruct
    private void init() {
        new Thread(() -> {
            for (Folder folder : folders) {
                add(folder);
            }
        }).start();
    }

    public void add(Content content) {
        if (content.getId() == null) {
            content.setId(idSequence++);
        }
        contents.put(content.getId(), content);
    }

    public void add(List<Content> content) {
        content.stream().forEach(this::add);
    }

}