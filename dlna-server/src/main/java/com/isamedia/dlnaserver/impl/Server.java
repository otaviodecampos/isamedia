package com.isamedia.dlnaserver.impl;

import com.isamedia.common.api.Startup;
import com.isamedia.common.impl.Settings;
import org.fourthline.cling.DefaultUpnpServiceConfiguration;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.model.meta.LocalDevice;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by otavio on 22/08/2016.
 */
@Startup
public class Server {

    @Inject
    private Settings settings;

    @Inject
    private LocalDevice defaultUpnpDevice;

    @PostConstruct
    private void init() {
        UpnpService upnpService = new UpnpServiceImpl(new DefaultUpnpServiceConfiguration(settings.getDlnaServerPort()));
        upnpService.getRegistry().addDevice(defaultUpnpDevice);
    }

}
