package com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual;

import com.isamedia.common.impl.i18n.I18nUtil;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOption;
import com.isamedia.dlnaserver.impl.content.folder.option.audio.SubtitleFolderBuilder;
import lombok.Data;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
@ContentOption({SubtitleFolderBuilder.class})
public class AudioFolder extends VirtualFolder {

    private final String name = I18nUtil.translate("audio");

}
