package com.isamedia.dlnaserver.impl.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * Created by otavio on 02/12/2016.
 */
@Data
public class VideoLanguageSubtitleItem extends VideoLanguageItem {

    private Integer externalIndex = -1;

    private String externalPath;

    @JsonIgnore
    public boolean isExternal() {
        return externalPath != null && !externalPath.isEmpty();
    }

}
