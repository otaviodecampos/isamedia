package com.isamedia.dlnaserver.impl.content.folder.option.video;

import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.GroupFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by otavio on 18/09/2016.
 */
public class GroupFolderBuilder implements ContentOptionBuilder<Media> {

    @Override
    public boolean isValid(Media media) {
        return !media.isDuplicated() && (media.getParent().getChildrenMedia().size() > 1 || hasGroupGolder(media.getParent()));
    }

    @Override
    public List<Content> build(Media media) {
        List<Content> contents = new ArrayList<>();
        List<Folder> options = getOwnerOptions(media);
        Folder parent = media.getParent();

        GroupFolder groupFolder = new GroupFolder(media);
        parent.addChildren(groupFolder);
        parent.removeChildren(options);
        parent.removeChildren(media);
        groupFolder.addChildren(options);
        groupFolder.addChildren(media);
        contents.add(groupFolder);

        return contents;
    }

    private List<Folder> getOwnerOptions(Media media) {
        return media.getParent().getChildrenVirtual().stream().filter(children -> children.getOwner().equals(media) && (children instanceof SubtitleFolder || children instanceof AudioFolder)).collect(Collectors.toList());
    }

    private boolean hasGroupGolder(Folder folder) {
        return folder.getChildrenVirtual().stream().filter(children -> children instanceof GroupFolder).limit(1).findFirst().isPresent();
    }

}
