package com.isamedia.dlnaserver.impl.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;

import java.io.IOException;

/**
 * Created by otavio on 22/10/2016.
 */
public class FolderSerializer extends StdSerializer<Folder> {

    public FolderSerializer() {
        this(null);
    }

    public FolderSerializer(Class<Folder> t) {
        super(t);
    }

    @Override
    public void serialize(Folder folder, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", folder.getId());
        jsonGenerator.writeStringField("name", folder.getName());
        jsonGenerator.writeNumberField("children", folder.getChildren().size());
        jsonGenerator.writeStringField("type", getType(folder));
        jsonGenerator.writeEndObject();
    }

    private String getType(Folder folder) {
        Class<?> clazz = ApplicationManager.isProxy(folder) ? folder.getClass().getSuperclass() : folder.getClass();
        return clazz.getSimpleName();
    }
}
