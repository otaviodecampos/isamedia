package com.isamedia.dlnaserver.impl.content.folder.type.video;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.common.impl.Settings;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOption;
import com.isamedia.dlnaserver.impl.content.folder.option.video.*;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.serializer.VideoSerializer;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
@AllArgsConstructor
@ContentOption({AudioFolderBuilder.class, SubtitleFolderBuilder.class, GroupFolderBuilder.class, QualityFolderBuilder.class, TimeFolderBuilder.class})
@JsonSerialize(using = VideoSerializer.class)
public class Video extends Media {

    @NonNull
    private VideoParams params;

    private List<VideoExternalSubtitle> externalSubtitles = new ArrayList();

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Settings settings = ApplicationManager.getBean(Settings.class);

    @Getter
    private String type = "Video";

    @Builder
    private Video(String name, String path, String mimeType, Folder parent, VideoParams params) {
        setName(name);
        setPath(path);
        setMimeType(mimeType);
        setParent(parent);

        if (params == null) {
            params = VideoParams.builder().build();
            if (settings.getMediaQuality() != null && params.getQuality() == null) {
                VideoQuality quality;
                try {
                    quality = VideoQuality.valueOf(settings.getMediaQuality().toUpperCase());
                } catch (Exception e) {
                    quality = VideoQuality.NONE;
                }
                params.setQuality(quality);
            }
        }

        setParams(params);
    }

    @Override
    public Video duplicate() {
        Video duplicate = super.duplicate();
        duplicate.setParams(duplicate.getParams().duplicate());
        return duplicate;
    }

    public Video findSameWithParams(VideoParams params) {
        Video selected = null;
        if(isSameParams(params)) {
            selected = this;
        } else {
            for (Folder folder : this.getParent().getChildrenVirtual()) {
                for (Folder childFolder : folder.getChildrenVirtual()) {
                    for (Video video : childFolder.getChildrenVideo()) {
                        if (video.isSameParams(params)) {
                            selected = video;
                            break;
                        }
                    }
                    if (selected != null) {
                        break;
                    }
                }
                if (selected != null) {
                    break;
                }
            }
        }
        return selected;
    }

    private boolean isSameParams(VideoParams params) {
        boolean hasExternalSubtitle = this.getParams().getExternalSubtitle() != null;
        if(hasExternalSubtitle) {
            return this.getParams().getAudioStream() == params.getAudioStream() && this.getParams().getSubtitleStream() == params.getSubtitleStream() && params.getExternalSubtitle() != null && this.getParams().getExternalSubtitle().equals(params.getExternalSubtitle());
        }
        return this.getParams().getAudioStream() == params.getAudioStream() && this.getParams().getSubtitleStream() == params.getSubtitleStream() && this.getParams().getExternalSubtitle() == null && params.getExternalSubtitle() == null;
    }

}
