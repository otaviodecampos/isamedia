package com.isamedia.dlnaserver.impl.content.folder.option.video;

import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.media.impl.analysis.type.MediaStream;

import java.util.ArrayList;
import java.util.List;

import static com.isamedia.dlnaserver.impl.content.folder.option.OptionHelper.buildAudioStreamOption;

/**
 * Created by otavio on 17/09/2016.
 */
public class AudioFolderBuilder implements ContentOptionBuilder<Video> {

    public boolean isValid(Video video) {
        return !video.isDuplicated() && video.analyse().getAudioStreams().size() > 1;
    }

    @Override
    public List<Content> build(Video video) {
        AudioFolder audioFolder = new AudioFolder();
        audioFolder.setOwner(video);

        List<Content> contents = new ArrayList<>();
        contents.add(audioFolder);

        Folder parent = video.getParent();
        parent.addChildren(audioFolder);

        for (MediaStream stream : video.analyse().getAudioStreams()) {
            contents.addAll(buildAudioStreamOption(audioFolder, video, stream));
        }

        return contents;
    }

}
