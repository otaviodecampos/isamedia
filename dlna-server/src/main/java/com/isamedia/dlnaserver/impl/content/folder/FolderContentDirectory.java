package com.isamedia.dlnaserver.impl.content.folder;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.converter.FolderConverter;
import com.isamedia.dlnaserver.impl.content.converter.MediaConverter;
import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import org.fourthline.cling.support.contentdirectory.AbstractContentDirectoryService;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryErrorCode;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryException;
import org.fourthline.cling.support.contentdirectory.DIDLParser;
import org.fourthline.cling.support.model.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 15/08/2016.
 */
public class FolderContentDirectory extends AbstractContentDirectoryService {

    @Inject
    private ContentManager database;

    @Inject
    private DIDLParser parser;

    @Inject
    private FolderConverter folderConverter;

    @Inject
    private MediaConverter mediaConverter;

    @Override
    public BrowseResult browse(String folderId, BrowseFlag browseFlag,
                               String filter,
                               long firstResult, long maxResults,
                               SortCriterion[] orderby) throws ContentDirectoryException {
        try {
            DIDLContent content = new DIDLContent();
            Content item = database.get(folderId);

            if (item instanceof Folder) {
                Folder folder = (Folder) item;
                if (browseFlag.equals(BrowseFlag.METADATA)) {
                    content.addContainer(folderConverter.convert(folder));
                } else {
                    List<DIDLObject> results = new ArrayList<>();

                    folder.getChildrenFolder().stream().forEach(children -> results.add(folderConverter.convert(children)));
                    folder.getChildrenMedia().stream().forEach(children -> results.add(mediaConverter.convert(children)));

                    maxResults = firstResult + maxResults;
                    if(maxResults > results.size() || maxResults == 1 || maxResults == 0) {
                        maxResults = results.size();
                    }
                    results.subList((int) firstResult, (int) maxResults).forEach(content::addObject);
                }
            } else {
                content.addItem(mediaConverter.convert((Media) item));
            }

            return new BrowseResult(parser.generate(content), content.getCount(), content.getCount());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ContentDirectoryException(
                    ContentDirectoryErrorCode.CANNOT_PROCESS,
                    ex.toString()
            );
        }
    }

    @Override
    public BrowseResult search(String containerId,
                               String searchCriteria, String filter,
                               long firstResult, long maxResults,
                               SortCriterion[] orderBy) throws ContentDirectoryException {
        return super.search(containerId, searchCriteria, filter, firstResult, maxResults, orderBy);
    }
}
