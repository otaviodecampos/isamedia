package com.isamedia.dlnaserver.impl.content.folder.type.audio;

import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Created by otavio on 11/09/2016.
 */
@AllArgsConstructor
public class MediaAudio extends Media {

    @Getter
    private String type = "Audio";

    @Builder
    private MediaAudio(String name, String path, String mimeType, Folder parent) {
        setName(name);
        setPath(path);
        setMimeType(mimeType);
        setParent(parent);
    }

}
