package com.isamedia.dlnaserver.impl.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;

import java.io.IOException;

/**
 * Created by otavio on 13/11/2016.
 */
public class VirtualFolderSerializer extends StdSerializer<VirtualFolder> {

    public VirtualFolderSerializer() {
        this(null);
    }

    public VirtualFolderSerializer(Class<VirtualFolder> t) {
        super(t);
    }

    @Override
    public void serialize(VirtualFolder folder, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", folder.getId());
        jsonGenerator.writeStringField("name", folder.getName());
        jsonGenerator.writeNumberField("children", folder.getChildren().size());
        jsonGenerator.writeStringField("type", getType(folder));
        jsonGenerator.writeNumberField("ownerId", folder.getOwner().getId());
        jsonGenerator.writeEndObject();
    }

    private String getType(Folder folder) {
        Class<?> clazz = ApplicationManager.isProxy(folder) ? folder.getClass().getSuperclass() : folder.getClass();
        return clazz.getSimpleName();
    }
}
