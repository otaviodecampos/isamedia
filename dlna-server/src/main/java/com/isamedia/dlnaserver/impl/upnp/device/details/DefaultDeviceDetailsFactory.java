package com.isamedia.dlnaserver.impl.upnp.device.details;

import com.isamedia.dlnaserver.api.upnp.device.DeviceDetailsFactory;
import com.isamedia.dlnaserver.impl.upnp.UpnpConstant;
import org.fourthline.cling.model.profile.HeaderDeviceDetailsProvider;

import java.util.*;

/**
 * Created by otavio on 24/08/2016.
 */
public class DefaultDeviceDetailsFactory implements DeviceDetailsFactory {

    @Override
    public String getModelName() {
        return UpnpConstant.MEDIA_SERVER_NAME;
    }

    @Override
    public String getModelDescription() {
        return UpnpConstant.MEDIA_SERVER_NAME;
    }

    @Override
    public String getModelNumber() {
        return "1.0";
    }

    @Override
    public List<HeaderDeviceDetailsProvider.Key> getHeaderDeviceDetailsProviderKeys() {
        return Arrays.asList(new HeaderDeviceDetailsProvider.Key("X-AV-Client-Info", ".*PLAYSTATION 3.*"));
    }

}
