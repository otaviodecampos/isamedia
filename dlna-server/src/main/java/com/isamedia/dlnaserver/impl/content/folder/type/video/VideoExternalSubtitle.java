package com.isamedia.dlnaserver.impl.content.folder.type.video;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by otavio on 02/10/2016.
 */
@Getter
@AllArgsConstructor
public class VideoExternalSubtitle {

    private final String name;

    private final String path;

}
