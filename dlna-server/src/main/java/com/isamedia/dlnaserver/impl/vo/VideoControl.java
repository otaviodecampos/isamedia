package com.isamedia.dlnaserver.impl.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 16/12/2016.
 */
@Data
@NoArgsConstructor
public class VideoControl {

    private int discardInitialSeconds;

    private List<VideoLanguageItem> audio = new ArrayList<>();

    private List<VideoLanguageSubtitleItem> subtitle = new ArrayList<>();

    public void addAudio(VideoLanguageItem audio) {
        this.audio.add(audio);
    }

    public void addSubtitle(VideoLanguageSubtitleItem subtitle) {
        this.subtitle.add(subtitle);
    }

    @JsonIgnore
    public VideoLanguageItem getSelectedAudio() {
        for (VideoLanguageItem languageAudio : audio) {
            if (languageAudio.isSelected()) {
                return languageAudio;
            }
        }
        return null;
    }

    @JsonIgnore
    public VideoLanguageSubtitleItem getSelectedSubtitle() {
        for (VideoLanguageSubtitleItem languageSubtitle : subtitle) {
            if (languageSubtitle.isSelected()) {
                return languageSubtitle;
            }
        }
        return null;
    }

}
