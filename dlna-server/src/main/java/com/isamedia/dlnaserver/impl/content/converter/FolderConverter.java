package com.isamedia.dlnaserver.impl.content.converter;

import com.isamedia.dlnaserver.api.contentdirectory.folder.converter.Converter;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import org.fourthline.cling.support.model.container.StorageFolder;

/**
 * Created by otavio on 25/08/2016.
 */
public class FolderConverter implements Converter<Folder, StorageFolder> {

    @Override
    public StorageFolder convert(Folder folder) {
        StorageFolder storageFolder = new StorageFolder();
        storageFolder.setId(String.valueOf(folder.getId()));
        storageFolder.setParentID(folder.getParent() != null ? String.valueOf(folder.getParent().getId()) : "-1");
        storageFolder.setTitle(folder.getName());
        storageFolder.setRestricted(true);
        storageFolder.setChildCount(folder.getChildren().size());
        return storageFolder;
    }

}
