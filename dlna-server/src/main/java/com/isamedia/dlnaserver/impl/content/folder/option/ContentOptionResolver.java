package com.isamedia.dlnaserver.impl.content.folder.option;

import com.google.common.collect.ImmutableList;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOption;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 11/09/2016.
 */
public class ContentOptionResolver {

    @Inject
    private Instance<ContentOptionBuilder> builders;

    public List<Content> resolve(Content content) {
        List<Content> contents = new ArrayList<>();
        ContentOption contentOption = content.getClass().getDeclaredAnnotation(ContentOption.class);

        if (content instanceof Media && ((Media) content).isInvalid()) {
            return contents;
        }

        if (contentOption != null) {
            Class<? extends ContentOptionBuilder>[] classes = contentOption.value();

            for (Class<? extends ContentOptionBuilder> builderClass : classes) {
                ContentOptionBuilder builder = builders.select(builderClass).get();

                if (builder.isValid(content)) {
                    try {
                        List<Content> options = builder.build(content);
                        contents.addAll(options != null ? options : new ArrayList<>());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        ImmutableList<Content> list = ImmutableList.copyOf(contents);
        list.forEach(builderContent -> contents.addAll(resolve(builderContent)));

        return contents;
    }

}