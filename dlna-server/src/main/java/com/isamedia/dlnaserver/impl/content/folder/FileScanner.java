package com.isamedia.dlnaserver.impl.content.folder;

import com.google.common.io.Files;
import com.isamedia.common.api.ActionFunction;
import com.isamedia.common.impl.Settings;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.type.audio.MediaAudio;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.MediaFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoExternalSubtitle;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

/**
 * Created by otavio on 26/08/2016.
 */
@Log4j2
public class FileScanner {

    @Inject
    private Settings settings;

    @Inject
    private MediaFolder mediaFolder;

    public void scan(ActionFunction<Content> actionFunction) {
        for (String scanPath : settings.getMediaFolders().split(",")) {
            scanFile(new File(scanPath), mediaFolder, actionFunction);
        }
    }

    private void scanFile(File file, Folder parent, ActionFunction actionFunction) {
        boolean take = true;
        Content content = null;

        if (file.isDirectory()) {
            content = buildFolder(file, parent);
            Folder folder = (Folder) content;

            for (File child : file.listFiles()) {
                scanFile(child, folder, actionFunction);
            }

            take = parent != null && !folder.getChildren().isEmpty();
        } else {
            String mimeType = FileMediaDetail.getMimeType(file);
            if (FileMediaDetail.isVideoMimeType(mimeType)) {
                content = buildMediaVideo(file, mimeType, parent);
            } else if (FileMediaDetail.isAudioMimeType(mimeType)) {
                content = buildMediaAudio(file, mimeType, parent);
            }
        }

        if (content != null && take) {
            parent.getChildren().add(content);
            actionFunction.action(content);
        }
    }

    private Folder buildFolder(File file, Folder parent) {
        return Folder.builder().parent(parent).name(file.getName()).path(file.getAbsolutePath()).build();
    }

    private Content buildMediaVideo(File file, String mimeType, Folder parent) {
        Video video = Video.builder().name(file.getName()).path(file.getAbsolutePath()).mimeType(mimeType).parent(parent).build();
        scanExternalSubtitles(video);
        return video;
    }

    private void scanExternalSubtitles(Video video) {
        String path = video.getPath();
        for (File subtitleFile : new File(path + "/..").listFiles((dir, name) -> name.endsWith(".srt"))) {
            try {
                String subtitlePath = subtitleFile.getCanonicalPath();
                VideoExternalSubtitle subtitle = new VideoExternalSubtitle(Files.getNameWithoutExtension(subtitlePath + ".srt"), subtitlePath);
                video.getExternalSubtitles().add(subtitle);
            } catch (IOException e) {
                log.error(e);
            }
        }
    }

    private Content buildMediaAudio(File file, String mimeType, Folder parent) {
        return MediaAudio.builder().name(file.getName()).path(file.getAbsolutePath()).mimeType(mimeType).parent(parent).build();
    }

}
