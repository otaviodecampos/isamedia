package com.isamedia.dlnaserver.impl;

import com.isamedia.dlnaserver.api.Notifier;

/**
 * Created by otavio on 17/09/2016.
 */
public class NotifierRunnable implements Runnable {

    private Notifier notifier;

    private Runnable runnable;

    public NotifierRunnable(Runnable runnable, Notifier notifier) {
        this.runnable = runnable;
        this.notifier = notifier;
    }

    @Override
    public void run() {
        try {
            runnable.run();
        } catch (Exception e) {
            notifier.notify(e);
        }
    }
}
