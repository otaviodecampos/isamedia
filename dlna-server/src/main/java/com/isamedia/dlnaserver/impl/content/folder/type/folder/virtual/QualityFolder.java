package com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual;

import com.isamedia.common.impl.i18n.I18nUtil;
import lombok.Data;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
public class QualityFolder extends VirtualFolder {

    private final String name = I18nUtil.translate("quality");

}
