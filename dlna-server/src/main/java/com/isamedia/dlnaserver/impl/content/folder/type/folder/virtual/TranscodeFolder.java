package com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual;

import lombok.Data;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
public class TranscodeFolder extends VirtualFolder {

    private final String name = "Transcode";

}
