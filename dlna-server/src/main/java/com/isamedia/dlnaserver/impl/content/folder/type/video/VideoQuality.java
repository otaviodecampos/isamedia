package com.isamedia.dlnaserver.impl.content.folder.type.video;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by otavio on 21/09/2016.
 */
@AllArgsConstructor
public enum VideoQuality {

    NONE("Original", 0),
    SD("SD", 576),
    HD("HD", 720),
    FULLHD("FULLHD", 1080);

    @Getter
    private String label;

    @Getter
    private int height;

}
