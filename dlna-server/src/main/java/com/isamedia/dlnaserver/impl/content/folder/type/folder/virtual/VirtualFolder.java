package com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.serializer.VirtualFolderSerializer;
import lombok.Data;

/**
 * Created by otavio on 11/09/2016.
 */
@Data
@JsonSerialize(using = VirtualFolderSerializer.class)
public class VirtualFolder extends Folder {

    private Media owner;

}
