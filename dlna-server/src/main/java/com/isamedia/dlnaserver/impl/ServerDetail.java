package com.isamedia.dlnaserver.impl;

import com.isamedia.common.impl.Settings;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.transport.spi.NetworkAddressFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by otavio on 27/08/2016.
 */
@ApplicationScoped
public class ServerDetail {

    @Inject
    private Settings settings;

    @Inject
    private UpnpService upnpService;

    private NetworkAddressFactory networkAddressFactory;

    @PostConstruct
    private void init() {
        networkAddressFactory = upnpService.getRouter().getConfiguration().createNetworkAddressFactory();
    }

    public String getHostAddress() {
        return networkAddressFactory.getBindAddresses().next().getHostAddress();
    }

    public String getHttpServerAddress() {
        return String.format("http://%s:%s", getHostAddress(), settings.getHttpServerPort());
    }

}
