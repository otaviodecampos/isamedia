package com.isamedia.dlnaserver.impl.content.converter;

/**
 * Created by otavio on 27/08/2016.
 */
public class ConvertException extends Exception {

    public ConvertException() {
        super();
    }

    public ConvertException(String message) {
        super(message);
    }

    public ConvertException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConvertException(Throwable cause) {
        super(cause);
    }

}
