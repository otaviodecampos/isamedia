package com.isamedia.dlnaserver.impl.upnp.device.service;

import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.support.connectionmanager.ConnectionManagerService;
import org.fourthline.cling.support.model.Protocol;
import org.fourthline.cling.support.model.ProtocolInfo;
import org.fourthline.cling.support.model.ProtocolInfos;

import javax.enterprise.inject.Produces;

/**
 * Created by otavio on 24/08/2016.
 */
public class ConnectionManagerServiceProducer {

    @Produces
    public LocalService create() {
        LocalService<ConnectionManagerService> connectionManager = new AnnotationLocalServiceBinder().read(ConnectionManagerService.class);
        connectionManager.setManager(createConnectionServiceManager(connectionManager));
        return connectionManager;
    }

    private DefaultServiceManager<ConnectionManagerService> createConnectionServiceManager(LocalService<ConnectionManagerService> connectionManagerService) {
        return new DefaultServiceManager<ConnectionManagerService>(connectionManagerService) {
            @Override
            protected ConnectionManagerService createServiceInstance() throws Exception {
                return new ConnectionManagerService(createProtocolInfos(), null);
            }
        };
    }

    private ProtocolInfos createProtocolInfos() {
        return new ProtocolInfos(
                new ProtocolInfo(
                        Protocol.HTTP_GET,
                        ProtocolInfo.WILDCARD,
                        "audio/mpeg",
                        "DLNA.ORG_PN=MP3;DLNA.ORG_OP=01"
                ),
                new ProtocolInfo(
                        Protocol.HTTP_GET,
                        ProtocolInfo.WILDCARD,
                        "video/mpeg",
                        "DLNA.ORG_PN=MPEG1;DLNA.ORG_OP=01;DLNA.ORG_CI=0"
                )
        );
    }

}
