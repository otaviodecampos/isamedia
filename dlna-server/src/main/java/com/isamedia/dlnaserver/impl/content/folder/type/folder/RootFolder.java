package com.isamedia.dlnaserver.impl.content.folder.type.folder;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.ContentFolder;
import lombok.Data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 * Created by otavio on 26/08/2016.
 */
@Data
@Default
@ContentFolder
@ApplicationScoped
public class RootFolder extends Folder {

    private final Integer id = 0;

    private final String name = "Root";

}
