package com.isamedia.dlnaserver.impl.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;

import java.io.IOException;

/**
 * Created by otavio on 22/10/2016.
 */
public class MediaSerializer extends StdSerializer<Media> {

    public MediaSerializer() {
        this(null);
    }

    public MediaSerializer(Class<Media> t) {
        super(t);
    }

    @Override
    public void serialize(Media media, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", media.getId());
        jsonGenerator.writeStringField("name", media.getName());
        jsonGenerator.writeStringField("mimeType", media.getMimeType());
        jsonGenerator.writeStringField("type", media.getType());
        jsonGenerator.writeEndObject();
    }
}
