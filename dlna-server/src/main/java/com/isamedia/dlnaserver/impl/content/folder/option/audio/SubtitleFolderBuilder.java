package com.isamedia.dlnaserver.impl.content.folder.option.audio;

import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.option.OptionHelper;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.GroupFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.VirtualFolder;
import com.isamedia.media.impl.analysis.type.MediaStream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 17/09/2016.
 */
public class SubtitleFolderBuilder implements ContentOptionBuilder<AudioFolder> {

    @Override
    public boolean isValid(AudioFolder audioFolder) {
        return audioFolder.getOwner().analyse().getSubtitleStreams().size() > 0 && !(audioFolder.getParent() instanceof VirtualFolder) || audioFolder.getParent() instanceof GroupFolder;
    }

    @Override
    public List<Content> build(AudioFolder audioFolder) {
        List<Content> contents = new ArrayList<>();

        List<Folder> languageFolders = audioFolder.getChildrenFolder();
        for (Folder languageFolder : languageFolders) {
            VirtualFolder folder = (VirtualFolder) languageFolder;
            Media owner = folder.getOwner();

            SubtitleFolder subtitleFolder = new SubtitleFolder();
            subtitleFolder.setOwner(owner);
            folder.addChildren(subtitleFolder);
            contents.add(subtitleFolder);

            for (MediaStream stream : owner.analyse().getSubtitleStreams()) {
                contents.addAll(OptionHelper.buildSubtitleStreamOption(subtitleFolder, (Video) owner, stream));
            }
        }

        return contents;
    }

}
