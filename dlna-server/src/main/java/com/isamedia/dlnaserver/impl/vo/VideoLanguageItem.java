package com.isamedia.dlnaserver.impl.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by otavio on 02/12/2016.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class VideoLanguageItem {

    @NonNull
    private int stream;

    @NonNull
    private String language;

    private boolean selected;

}
