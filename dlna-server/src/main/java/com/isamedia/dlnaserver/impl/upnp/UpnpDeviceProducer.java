package com.isamedia.dlnaserver.impl.upnp;

import com.google.common.collect.Iterables;
import com.isamedia.dlnaserver.api.upnp.device.DeviceDetailsFactory;
import com.isamedia.dlnaserver.impl.upnp.device.details.DefaultDeviceDetailsFactory;
import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.DeviceIdentity;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.profile.HeaderDeviceDetailsProvider;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by otavio on 24/08/2016.
 */
public class UpnpDeviceProducer {

    @Inject
    private Instance<LocalService> localServices;

    @Inject
    private Instance<DeviceDetailsFactory> deviceDetailsFactories;

    @Inject
    private DefaultDeviceDetailsFactory defaultDeviceDetailsFactory;

    @Produces
    public LocalDevice create() throws ValidationException {
        return createUPnPDevice();
    }

    public LocalDevice createUPnPDevice() throws ValidationException, LocalServiceBindingException {
        Map<HeaderDeviceDetailsProvider.Key, DeviceDetails> headerDetails = new HashMap<>();

        for (DeviceDetailsFactory factory : deviceDetailsFactories) {
            for (HeaderDeviceDetailsProvider.Key key : factory.getHeaderDeviceDetailsProviderKeys()) {
                headerDetails.put(key, factory.createDeviceDetails());
            }
        }

        return new LocalDevice(
                new DeviceIdentity(UDN.uniqueSystemIdentifier(UpnpConstant.MEDIA_SERVER_NAME)),
                new UDADeviceType(UpnpConstant.DEVICE_TYPE, 1),
                new HeaderDeviceDetailsProvider(defaultDeviceDetailsFactory.createDeviceDetails(), headerDetails),
                null,
                Iterables.toArray(localServices, LocalService.class)
        );
    }


}
