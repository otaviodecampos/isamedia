package com.isamedia.dlnaserver.impl.content.folder.type.video;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by otavio on 12/09/2016.
 */
@Data
@AllArgsConstructor
public class VideoParams implements Cloneable {

    private VideoQuality quality;

    private Integer videoStream;

    private Integer audioStream;

    private Integer subtitleStream;

    private String externalSubtitle;

    private Boolean subtitleGraphic;

    private Integer discardInitialSeconds;

    @Builder
    private VideoParams(Integer audioStream, Integer subtitleStream, String externalSubtitle) {
        this.audioStream = audioStream != null ? audioStream : 0;
        this.subtitleStream = subtitleStream != null ? subtitleStream : -1;
        this.externalSubtitle = externalSubtitle;
    }

    @Builder
    private VideoParams(Integer videoStream, Integer audioStream, Integer subtitleStream, Integer discardInitialSeconds, Boolean subtitleGraphic, VideoQuality quality) {
        this.videoStream = videoStream != null ? videoStream : 0;
        this.audioStream = audioStream != null ? audioStream : 0;
        this.subtitleStream = subtitleStream != null ? subtitleStream : -1;
        this.discardInitialSeconds = discardInitialSeconds != null ? discardInitialSeconds : 0;
        this.subtitleGraphic = subtitleGraphic != null ? subtitleGraphic : false;
        this.quality = quality;
    }

    public boolean hasSubtitle() {
        return subtitleStream != -1;
    }

    public boolean hasExternalSubtitle() {
        return externalSubtitle != null;
    }

    public boolean hasSubtitleGraphic() {
        return subtitleGraphic != null && subtitleGraphic;
    }

    public boolean hasDiscardTime() {
        return discardInitialSeconds != null && discardInitialSeconds != 0;
    }

    public VideoParams duplicate() {
        try {
            return (VideoParams) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return this;
        }
    }

}
