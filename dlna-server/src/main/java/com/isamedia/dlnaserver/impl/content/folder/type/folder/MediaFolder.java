package com.isamedia.dlnaserver.impl.content.folder.type.folder;

import com.google.common.collect.ImmutableList;
import com.isamedia.common.impl.i18n.I18nUtil;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.ContentFolder;
import com.isamedia.dlnaserver.api.LogMessage;
import com.isamedia.dlnaserver.impl.NotifierRunnable;
import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.FileScanner;
import com.isamedia.dlnaserver.impl.content.folder.option.ContentOptionResolver;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 26/08/2016.
 */
@Log4j2
@Data
@Default
@ContentFolder
@ApplicationScoped
public class MediaFolder extends Folder {

    private final Integer id = 1;

    private final String name = I18nUtil.translate("media");

    private List<Content> contents = new ArrayList<>();

    @Inject
    private RootFolder parent;

    @Inject
    private ContentManager database;

    @Inject
    private FileScanner scanner;

    @Inject
    private ContentOptionResolver resolver;

    @PostConstruct
    private void init() {
        parent.addChildren(this);
        scan();
    }

    public void scan() {
        new Thread(new NotifierRunnable(() -> {
            scanner.scan(content -> contents.add(content));
            analyze();
            log.info(LogMessage.DATABASE_SCAN_FINISHED);
        }, e -> e.printStackTrace())).start();
    }

    private void analyze() {
        new Thread(new NotifierRunnable(() -> {
            ImmutableList<Content> list = ImmutableList.copyOf(contents);
            list.stream().filter(content -> content instanceof Media).map(content -> (Media) content).forEach(media -> media.analyse());
            list.stream().forEach(content -> resolver.resolve(content).stream().forEach(option -> contents.add(option)));
            database.add(contents);
            log.info(LogMessage.DATABASE_ANALYSIS_FINISHED);
        }, e -> e.printStackTrace())).start();
    }

}
