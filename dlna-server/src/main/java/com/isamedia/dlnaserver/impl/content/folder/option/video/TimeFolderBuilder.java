package com.isamedia.dlnaserver.impl.content.folder.option.video;

import com.isamedia.common.impl.Settings;
import com.isamedia.dlnaserver.api.contentdirectory.folder.option.ContentOptionBuilder;
import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.folder.MediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.AudioFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.QualityFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.SubtitleFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.virtual.TimeFolder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import lombok.NoArgsConstructor;

import javax.inject.Inject;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 18/09/2016.
 */
@NoArgsConstructor
public class TimeFolderBuilder implements ContentOptionBuilder<Video> {

    @Inject
    private MediaDetail mediaDetail;

    @Inject
    private Settings settings;

    @Override
    public boolean isValid(Video video) {
        return video.analyse().getFormat().getDuration() != null && (!video.isDuplicated() || (video.getParent() != null && (video.getParent().getParent() instanceof AudioFolder || video.getParent().getParent() instanceof SubtitleFolder || video.getParent().getParent() instanceof QualityFolder) && !hasChapterFolder(video.getParent())));
    }

    @Override
    public List<Content> build(Video video) {
        List<Content> contents = new ArrayList<>();

        TimeFolder folder = new TimeFolder();
        video.getParent().addChildren(folder);
        folder.setOwner(video);
        contents.add(folder);

        for (LocalTime time : createTimes(video)) {
            Video timeOption = video.duplicate();
            timeOption.setName(createTimeName(time, video));
            timeOption.getParams().setDiscardInitialSeconds(time.toSecondOfDay());
            folder.addChildren(timeOption);
            contents.add(timeOption);
        }

        return contents;
    }

    private List<LocalTime> createTimes(Video video) {
        List<LocalTime> times = new ArrayList();
        LocalTime time = LocalTime.of(0, 0);
        LocalTime maxTime = mediaDetail.getDurationTime(video);
        while (time.isBefore(maxTime)) {
            times.add(time);
            time = time.plusMinutes(settings.getTimeInterval());
        }
        return times;
    }

    private String createTimeName(LocalTime time, Video video) {
        String template = "[%s] %s";
        return String.format(template, time.format(DateTimeFormatter.ofPattern("HH:mm:ss")), video.getName());
    }

    private boolean hasChapterFolder(Folder folder) {
        return folder.getChildrenVirtual().stream().filter(children -> children instanceof TimeFolder).limit(1).findFirst().isPresent();
    }

}
