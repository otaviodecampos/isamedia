package com.isamedia.dlnaserver.impl.vo;

import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by otavio on 08/11/2016.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class VideoCategory {

    @NonNull
    private String name;

    @NonNull
    private String folderPath;

    private List<Video> videos = new ArrayList<>();

}
