package com.isamedia.common.api;

/**
 * Created by otavio on 29/08/2016.
 */
public interface MediaTranscode {

    String EVER = "ever";

    String SUBTITLE_FOUND = "subtitle";

    String REQUESTED = "requested";

}
