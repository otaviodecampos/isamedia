package com.isamedia.common.api;

/**
 * Created by otavio on 22/08/2016.
 */
public interface CodeBlock {

    void run();

}
