package com.isamedia.common.api;

/**
 * Created by otavio on 26/08/2016.
 */
public interface ActionFunction<T> {

    void action(T take);

}
