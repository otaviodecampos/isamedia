package com.isamedia.common.api;

/**
 * Created by otavio on 23/11/2016.
 */
public interface MediaType {

    String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";

}
