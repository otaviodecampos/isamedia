package com.isamedia.common.impl.util;

import com.google.common.io.Files;

/**
 * Created by otavio on 11/09/2016.
 */
public class FilenameUtil {

    public static String removeExtension(String fileName) {
        String extension = "." + Files.getFileExtension(fileName);
        return fileName.replaceAll(extension, "");
    }

    public static String getFileExtension(String fileName) {
        return "." + Files.getFileExtension(fileName);
    }

}
