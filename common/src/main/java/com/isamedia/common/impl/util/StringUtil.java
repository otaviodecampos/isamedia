package com.isamedia.common.impl.util;

/**
 * Created by otavio on 11/09/2016.
 */
public class StringUtil {

    public static String capitalize(final String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

}
