package com.isamedia.common.impl.i18n;

import org.apache.deltaspike.core.api.message.MessageContext;
import org.apache.deltaspike.core.api.message.MessageResolver;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Created by otavio on 12/10/2016.
 */
public class I18nTranslator {

    @Inject
    private MessageContext context;

    @Inject
    private MessageResolver resolver;

    @PostConstruct
    private void init() {
        context.messageSource("i18n.messages");
    }

    public String translate(String key) {
        return resolver.getMessage(context, String.format("{%s}", key), null);
    }

}
