package com.isamedia.common.impl.i18n;

import com.isamedia.common.impl.ApplicationManager;

/**
 * Created by otavio on 12/10/2016.
 */
public class I18nUtil {

    public static String translate(String key) {
        I18nTranslator translator = ApplicationManager.getBean(I18nTranslator.class);
        return translator.translate(key);
    }

}
