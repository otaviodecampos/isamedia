package com.isamedia.common.impl;

import com.isamedia.common.api.MediaTranscode;
import lombok.Data;
import org.apache.deltaspike.core.api.config.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by otavio on 29/08/2016.
 */
@Data
@ApplicationScoped
public class Settings {

    @Inject
    @ConfigProperty(name = "http.server.port")
    private Integer httpServerPort = 9998;

    @Inject
    @ConfigProperty(name = "dlna.server.port")
    private Integer dlnaServerPort = 8895;

    @Inject
    @ConfigProperty(name = "media.folders")
    private String mediaFolders;

    @Inject
    @ConfigProperty(name = "media.transcode")
    private String mediaTranscode = MediaTranscode.EVER;

    @Inject
    @ConfigProperty(name = "media.transcode.default.quality")
    private String mediaQuality = "HD";

    @Inject
    @ConfigProperty(name = "media.transcode.option.time.interval")
    private Integer timeInterval = 5;

    @Inject
    @ConfigProperty(name = "media.transcode.quality.sd")
    private String transcodeQualitySd = "25";

    @Inject
    @ConfigProperty(name = "media.transcode.quality.hd")
    private String transcodeQualityHd  = "23";

    @Inject
    @ConfigProperty(name = "media.transcode.quality.fullhd")
    private String transcodeQualityFullHd = "23";

    @Inject
    @ConfigProperty(name = "media.transcode.log")
    private String transcodeLog = "error";

    @Inject
    @ConfigProperty(name = "media.transcode.quality.sd.compression")
    private String transcodeQualitySdCompression = "fast";

    @Inject
    @ConfigProperty(name = "media.transcode.quality.hd.compression")
    private String transcodeQualityHdCompression = "fast";

    @Inject
    @ConfigProperty(name = "media.transcode.quality.fullhd.compression")
    private String transcodeQualityFullHdCompression = "fast";

    @Inject
    @ConfigProperty(name = "media.transcode.with.subtitlegraphic.compression")
    private String transcodeQualityWithSubtitleGraphicCompression = "veryfast";

}
