package com.isamedia.common.impl;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import javax.enterprise.context.ApplicationScoped;
import java.util.Map;

/**
 * Created by otavio on 09/10/2016.
 */
@ApplicationScoped
public class PersistenceManager {

    private final String DATABASE_FILE_NAME = "isabase.db";

    private DB database = DBMaker.fileDB(DATABASE_FILE_NAME).transactionEnable().make();

    public <K, T> Map<K, T> get(String name) {
        return (Map<K, T>) database.hashMap(name).createOrOpen();
    }

    public <K, T> Map<K, T> get(String name, Serializer valueSerializer) {
        return (Map<K, T>) database.hashMap(name).counterEnable().valueSerializer(valueSerializer).createOrOpen();
    }

    public void commit() {
        database.commit();
    }

}
