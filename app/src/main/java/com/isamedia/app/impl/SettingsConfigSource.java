package com.isamedia.app.impl;

import com.isamedia.httpserver.impl.util.PropertiesReader;
import lombok.extern.log4j.Log4j2;
import org.apache.deltaspike.core.impl.config.PropertiesConfigSource;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import static com.isamedia.app.api.LogMessage.LOAD_SETTINGS_PROPERTIES;

/**
 * Created by otavio on 27/08/2016.
 */
@Log4j2
public class SettingsConfigSource extends PropertiesConfigSource {

    private static String SETTINGS_FILE_NAME = "settings.properties";

    public SettingsConfigSource() {
        super(loadProperties());
    }

    private static Properties loadProperties() {
        Path currentRelativePath = Paths.get("");
        String workingDirectoryPath = currentRelativePath.toAbsolutePath().toString();
        String settingsFilePath = workingDirectoryPath + File.separator + SETTINGS_FILE_NAME;
        log.info(LOAD_SETTINGS_PROPERTIES, settingsFilePath);
        return PropertiesReader.loadProperties(new File(settingsFilePath));
    }

    @Override
    public String getConfigName() {
        return "SettingsConfig";
    }
}
