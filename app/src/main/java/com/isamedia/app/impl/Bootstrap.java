package com.isamedia.app.impl;

import com.isamedia.common.impl.ApplicationManager;

/**
 * Created by otavio on 22/08/2016.
 */
public class Bootstrap {

    public static void main(String[] args) {
        ApplicationManager.boot();
    }

}

