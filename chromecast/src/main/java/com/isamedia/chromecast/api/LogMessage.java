package com.isamedia.chromecast.api;

/**
 * Created by otavio on 27/11/2016.
 */
public interface LogMessage {

    String CHROMECAST_ADDED = "Chromecast {} adicionado.";

    String CHROMECAST_CONNECT = "Conectando ao chromecast {}.";

    String CHROMECAST_CONNECTED = "Conectado ao chromecast {} com sucesso.";

    String CHROMECAST_RUN_RECEIVER = "Iniciando player no chromecast {}.";

    String CHROMECAST_DISCONNECT = "Desconectando do chromecast {}.";

    String CHROMECAST_LOAD = "Carregando no chromecast: {}, media: {}. ";

    String CHROMECAST_LOAD_NOT_CONNECTED = "Não foi possível carregar midia porque não há dispositivo conectado.";

    String CHROMECAST_PLAY_NOT_CONNECTED = "Não foi possível iniciar reprodução porque não há dispositivo conectado.";

    String CHROMECAST_PAUSE_NOT_CONNECTED = "Não foi possível pausar reprodução porque não há dispositivo conectado.";

    String CHROMECAST_REMOVED = "Chromecast removido {}";

    String CHROMECAST_PLAY = "Iniciar reprodução no chromecast {}.";

    String CHROMECAST_PAUSE = "Pausar reprodução no chromecast {}.";

    String CHROMECAST_NOT_CONECTABLE = "Chromecast não é conectavel.";

}
