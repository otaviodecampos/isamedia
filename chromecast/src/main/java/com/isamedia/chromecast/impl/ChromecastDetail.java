package com.isamedia.chromecast.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by otavio on 24/11/2016.
 */
@Log4j2
public class ChromecastDetail {

    private static final String DETAIL_URL_PATTERN = "http://%s:8008/setup/eureka_info?options=detail";

    public static Map<String, Object> get(String address) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new URL(String.format(DETAIL_URL_PATTERN, address)), Map.class);
        } catch (IOException e) {
            log.debug(e);
        }
        return new HashMap();
    }

}
