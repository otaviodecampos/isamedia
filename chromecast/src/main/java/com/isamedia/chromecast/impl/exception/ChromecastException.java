package com.isamedia.chromecast.impl.exception;

/**
 * Created by otavio on 20/11/2016.
 */
public class ChromecastException extends Exception {

    public ChromecastException(String s) {
        super(s);
    }

}
