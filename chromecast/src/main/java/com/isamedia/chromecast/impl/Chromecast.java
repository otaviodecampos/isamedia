package com.isamedia.chromecast.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import su.litvak.chromecast.api.v2.ChromeCast;

import java.util.Map;

/**
 * Created by otavio on 26/10/2016.
 */
@Log4j2
public class Chromecast {

    @Getter
    @JsonIgnore
    private ChromeCast chromeCastApi;

    @Getter
    private String name;

    @JsonIgnore
    private Map<String, Object> details;

    @Getter
    private ChromecastStatus status;

    public Chromecast(ChromeCast chromeCastApi) {
        this.chromeCastApi = chromeCastApi;
        this.name = chromeCastApi.getName();
        this.status = new ChromecastStatus(this);
        this.details = ChromecastDetail.get(this.chromeCastApi.getAddress());
    }

    public String getFriendlyName() {
        return (String) details.get("name");
    }

    @JsonIgnore
    public boolean isConnectable() {
        return !ChromecastDetail.get(this.chromeCastApi.getAddress()).isEmpty();
    }

}
