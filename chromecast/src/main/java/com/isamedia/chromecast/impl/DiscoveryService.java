package com.isamedia.chromecast.impl;

import com.isamedia.common.api.Startup;
import com.isamedia.chromecast.impl.listener.DiscoveryListener;
import lombok.extern.log4j.Log4j2;
import su.litvak.chromecast.api.v2.ChromeCasts;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by otavio on 26/10/2016.
 */
@Log4j2
@Default
@Startup
@ApplicationScoped
public class DiscoveryService {

    private static int DISCOVERY_TIMEOUT = 3000;

    @Inject
    private DiscoveryListener listener;

    @PostConstruct
    private void init() throws IOException {
        discovery();
        new Thread(() -> {
            try {
                Thread.sleep(DISCOVERY_TIMEOUT);
                stopDiscovery();
            } catch (InterruptedException | IOException e) {
                log.error(e);
            }
        }).start();
    }

    private void discovery() throws IOException {
        ChromeCasts.registerListener(listener);
        ChromeCasts.startDiscovery();
    }

    public void waitDiscovery() throws IOException, InterruptedException {
        waitDiscovery(DISCOVERY_TIMEOUT);
    }

    public void waitDiscovery(int timeout) throws IOException, InterruptedException {
        discovery();
        Thread.sleep(timeout);
        stopDiscovery();
    }

    private void stopDiscovery() throws IOException {
        ChromeCasts.stopDiscovery();
        ChromeCasts.unregisterListener(listener);
    }

}
