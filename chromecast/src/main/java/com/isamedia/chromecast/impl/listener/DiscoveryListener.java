package com.isamedia.chromecast.impl.listener;

import com.isamedia.chromecast.impl.ChromecastManager;
import su.litvak.chromecast.api.v2.ChromeCast;
import su.litvak.chromecast.api.v2.ChromeCastsListener;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by otavio on 26/10/2016.
 */
@ApplicationScoped
public class DiscoveryListener implements ChromeCastsListener {

    @Inject
    private ChromecastManager manager;

    @Override
    public void newChromeCastDiscovered(ChromeCast chromeCastApi) {
        manager.add(chromeCastApi);
    }

    @Override
    public void chromeCastRemoved(ChromeCast chromeCastApi) {
        manager.remove(chromeCastApi);
    }

}
