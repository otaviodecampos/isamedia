package com.isamedia.chromecast.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.log4j.Log4j2;
import su.litvak.chromecast.api.v2.Application;
import su.litvak.chromecast.api.v2.MediaStatus;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by otavio on 28/10/2016.
 */
@Log4j2
public class ChromecastStatus {

    private static final String MEDIA_ID_PATTERN = "(media\\/)(\\d+)";

    @JsonIgnore
    private Chromecast chromecast;

    public ChromecastStatus(Chromecast chromecast) {
        this.chromecast = chromecast;
    }

    public boolean isConnected() {
        return chromecast.getChromeCastApi().isConnected();
    }

    public String getRunningAppId() {
        String id = null;
        if (chromecast.isConnectable() && chromecast.getChromeCastApi().isConnected()) {
            try {
                Application app = chromecast.getChromeCastApi().getRunningApp();
                if (app != null) {
                    id = app.id;
                }
            } catch (IOException e) {
                log.error(e);
            }
        }
        return id;
    }

    public String getState() {
        String state = null;
        if (chromecast.isConnectable() && chromecast.getChromeCastApi().isConnected()) {
            try {
                MediaStatus status = chromecast.getChromeCastApi().getMediaStatus();
                if (status != null) {
                    state = status.playerState.name();
                }
            } catch (IOException e) {
                log.error(e);
            }
        }
        return state;
    }

    public Integer getMediaId() {
        Integer id = null;
        if (chromecast.isConnectable() && chromecast.getChromeCastApi().isConnected()) {
            try {
                MediaStatus status = chromecast.getChromeCastApi().getMediaStatus();
                if (status != null && status.media != null) {
                    Pattern r = Pattern.compile(MEDIA_ID_PATTERN);
                    Matcher m = r.matcher(status.media.url);
                    if (m.find()) {
                        id = Integer.parseInt(m.group(2));
                    }
                }
            } catch (IOException e) {
                log.error(e);
            }
        }
        return id;
    }

    public Integer getCurrentTime() {
        Integer currentTime = 0;
        if (chromecast.isConnectable() && chromecast.getChromeCastApi().isConnected()) {
            try {
                MediaStatus status = chromecast.getChromeCastApi().getMediaStatus();
                if (status != null && status.media != null) {
                    currentTime = Double.valueOf(status.currentTime).intValue();
                }
            } catch (IOException e) {
                log.error(e);
            }
        }
        return currentTime;
    }

}
