package com.isamedia.chromecast.impl.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.isamedia.chromecast.impl.Chromecast;

import java.io.IOException;

/**
 * Created by otavio on 26/10/2016.
 */
public class ChromecastSerializer extends StdSerializer<Chromecast> {

    public ChromecastSerializer() {
        this(null);
    }

    protected ChromecastSerializer(Class<Chromecast> t) {
        super(t);
    }

    @Override
    public void serialize(Chromecast chromecast, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        /*jsonGenerator.writeStringField("name", chromecast.getName());
        jsonGenerator.writeBooleanField("connected", chromecast.isConnected());
        if (chromecast.isConnected()) {
            jsonGenerator.writeStringField("runningAppId", chromecast.getChromeCastApi().getRunningApp().id);
            MediaStatus status = chromecast.getChromeCastApi().getMediaStatus();
            if (status != null) {
                jsonGenerator.writeStringField("state", status.playerState.name());
            }

        }*/
    }
}
