package com.isamedia.chromecast.impl.exception;

import com.isamedia.chromecast.api.LogMessage;

/**
 * Created by otavio on 20/11/2016.
 */
public class ChromecastNotConectableException extends ChromecastException {

    public ChromecastNotConectableException() {
        super(LogMessage.CHROMECAST_NOT_CONECTABLE);
    }

}
