package com.isamedia.chromecast.impl.resource;

import com.isamedia.chromecast.impl.Chromecast;
import com.isamedia.chromecast.impl.ChromecastManager;
import com.isamedia.chromecast.impl.exception.ChromecastNotConectableException;
import com.isamedia.common.api.MediaType;
import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.MediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.vo.VideoControl;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;

/**
 * Created by otavio on 26/10/2016.
 */
@Path("chromecast")
public class ChromecastResource {

    @Inject
    private ContentManager contentManager;

    @Inject
    private ChromecastManager chromecastManager;

    @Inject
    private MediaDetail mediaDetail;

    @GET
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public Collection<Chromecast> list() throws IOException, InterruptedException {
        return chromecastManager.get();
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public Chromecast get(@PathParam("name") String name) throws ChromecastNotConectableException {
        return chromecastManager.get(name);
    }

    @GET
    @Path("/{name}/connect")
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public Chromecast connect(@PathParam("name") String name) throws IOException, GeneralSecurityException {
        return chromecastManager.connectAndRun(name);
    }

    @GET
    @Path("/connected")
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public Chromecast connected() {
        return chromecastManager.getConnected();
    }

    @GET
    @Path("/disconnect")
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public void disconnect(@PathParam("name") String name) throws IOException {
        chromecastManager.disconnect();
    }

    @GET
    @Path("/refresh")
    @Produces(MediaType.APPLICATION_JSON_UTF8)
    public Collection<Chromecast> discovery(@PathParam("name") String name) throws IOException, InterruptedException {
        chromecastManager.discover();
        return list();
    }

    @POST
    @Path("/media/{id}")
    @Consumes(MediaType.APPLICATION_JSON_UTF8)
    public void load(@PathParam("id") int id, VideoControl videoControl) throws IOException, GeneralSecurityException {
        Video video = contentManager.get(id);
        String url = mediaDetail.getVideoHttpAddress(video, videoControl);
        chromecastManager.load(video.getName(), url);
    }

    @GET
    @Path("/play")
    public void play() throws IOException {
        chromecastManager.play();
    }

    @GET
    @Path("/pause")
    public void pause() throws IOException {
        chromecastManager.pause();
    }

}
