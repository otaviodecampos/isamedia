package com.isamedia.chromecast.impl;

import com.isamedia.chromecast.api.ApplicationId;
import com.isamedia.chromecast.api.LogMessage;
import com.isamedia.chromecast.impl.exception.ChromecastNotConectableException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import su.litvak.chromecast.api.v2.ChromeCast;
import su.litvak.chromecast.api.v2.Status;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by otavio on 26/10/2016.
 */
@Log4j2
@ApplicationScoped
public class ChromecastManager {

    Map<String, Chromecast> chromecasts = new ConcurrentHashMap<>();

    @Getter
    private Chromecast connected;

    @Inject
    private DiscoveryService discoveryService;

    public void discover() {
        chromecasts.clear();
        try {
            discoveryService.waitDiscovery(1000);
        } catch (IOException | InterruptedException e) {
            log.debug(e);
        }
        if(chromecasts.isEmpty()) {
            connected = null;
        }
    }

    public void add(ChromeCast chromeCastApi) {
        Chromecast chromecast = new Chromecast(chromeCastApi);
        remove(chromeCastApi.getName());
        chromecasts.put(chromeCastApi.getName(), chromecast);
        log.info(LogMessage.CHROMECAST_ADDED, chromecast.getFriendlyName());

        try {
            if (connected != null && chromeCastApi.getName().equals(connected.getName())) {
                connectAndRun(chromeCastApi.getName());
            }
        } catch (IOException | GeneralSecurityException e) {
            log.debug(e);
        }

    }

    public void remove(String name) {
        Chromecast chromecast = chromecasts.get(name);
        if(chromecast != null) {
            remove(chromecast.getChromeCastApi());
        }
    }

    public void remove(ChromeCast chromeCastApi) {
        Chromecast chromecast = chromecasts.get(chromeCastApi.getName());
        if (chromecast == connected) {
            try {
                disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        log.info(LogMessage.CHROMECAST_REMOVED, chromecast.getFriendlyName());
        chromecasts.remove(chromeCastApi.getName());
    }

    public Collection<Chromecast> get() {
        List<Chromecast> list = new ArrayList<>();
        if(!chromecasts.isEmpty()) {
            for(Chromecast chromecast : chromecasts.values()) {
                try {
                    list.add(get(chromecast.getName()));
                } catch (ChromecastNotConectableException e) {
                    discover();
                }
            }
        } else {
            discover();
        }
        return chromecasts.values();
    }

    public Chromecast getConnected() {
        if(connected != null && !connected.isConnectable() && !connected.getChromeCastApi().isConnected()) {
            discover();
        }
        return connected;
    }

    public Chromecast get(String name) throws ChromecastNotConectableException {
        Chromecast chromecast = chromecasts.get(name);
        if(!chromecast.isConnectable()) {
            throw new ChromecastNotConectableException();
        }
        if(chromecast == connected && !chromecast.getChromeCastApi().isConnected()) {
            discover();
        }
        return chromecast;
    }

    public Chromecast connect(String name) throws IOException, GeneralSecurityException {
        Chromecast chromecast = chromecasts.get(name);
        ChromeCast api = chromecasts.get(name).getChromeCastApi();
        log.info(LogMessage.CHROMECAST_CONNECT, chromecast.getFriendlyName());
        api.connect();
        connected = chromecast;
        log.info(LogMessage.CHROMECAST_CONNECTED, chromecast.getFriendlyName());
        return chromecast;
    }

    public Chromecast connectAndRun(String name) throws IOException, GeneralSecurityException {
        Chromecast chromecast = chromecasts.get(name);
        ChromeCast api = chromecast.getChromeCastApi();

        if (!api.isConnected()) {
            connect(name);
        }

        Status status = api.getStatus();
        if (api.isAppAvailable(ApplicationId.DEFAULT_MEDIA_RECEIVER_ID) && !status.isAppRunning(ApplicationId.DEFAULT_MEDIA_RECEIVER_ID)) {
            log.info(LogMessage.CHROMECAST_RUN_RECEIVER, chromecast.getFriendlyName());
            api.launchApp(ApplicationId.DEFAULT_MEDIA_RECEIVER_ID);
        }

        return connected;
    }

    public void disconnect() throws IOException {
        if (connected != null) {
            log.info(LogMessage.CHROMECAST_DISCONNECT, connected.getFriendlyName());
            connected.getChromeCastApi().disconnect();
            connected = null;
        }
    }

    public void load(String title, String url) throws IOException, GeneralSecurityException {
        if (connected != null) {
            ChromeCast api = connected.getChromeCastApi();
            log.info(LogMessage.CHROMECAST_LOAD, connected.getFriendlyName(), url);
            api.load(title, null, url, "video/x-matroska");
        } else {
            log.info(LogMessage.CHROMECAST_LOAD_NOT_CONNECTED);
        }
    }

    public void play() throws IOException {
        if (connected != null) {
            ChromeCast api = connected.getChromeCastApi();
            log.info(LogMessage.CHROMECAST_PLAY, connected.getFriendlyName());
            api.play();
        } else {
            log.info(LogMessage.CHROMECAST_PLAY_NOT_CONNECTED);
        }
    }

    public void pause() throws IOException {
        if (connected != null) {
            ChromeCast api = connected.getChromeCastApi();
            log.info(LogMessage.CHROMECAST_PAUSE, connected.getFriendlyName());
            api.pause();
        } else {
            log.info(LogMessage.CHROMECAST_PAUSE_NOT_CONNECTED);
        }
    }

}
