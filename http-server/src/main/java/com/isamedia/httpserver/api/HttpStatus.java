package com.isamedia.httpserver.api;

/**
 * Created by otavio on 29/08/2016.
 */
public interface HttpStatus {

    int OK = 200;

    int PARTIAL_CONTENT = 206;

}
