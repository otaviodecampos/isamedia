package com.isamedia.httpserver.api;

/**
 * Created by otavio on 28/08/2016.
 */
public interface HttpHeader {

    String ACCEPT_RANGES = "Accept-Ranges";

    String RANGE = "Range";

    String CONTENT_RANGE = "Content-Range";

    String  ETAG = "ETag";

    String CONNECTION = "Connection";

    String USER_AGENT = "user-agent";

}
