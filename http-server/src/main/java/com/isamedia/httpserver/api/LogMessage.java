package com.isamedia.httpserver.api;

/**
 * Created by otavio on 26/10/2016.
 */
public interface LogMessage {

    String START = "Streaming de media {} iniciado. Consumer {}";

    String FINISH = "Streaming de media {} finalizado.";

    String REQUEST_RANGE = "Requested range {} of media {}. Consumer {}";

    String BUFFERING = "Buffering {} of media {}. Consumer {}";

    String BUFFER_RESETS = "Streaming media {} buffer resets";

    String SENDING_DATA_RANGE = "Sending data range of media {}. From {} to {}. Consumer {}";

    String CLOSE = "Client closed media {} connection. Consumer {}";

    String REQUEST_MEDIA = "Solicitação de media: {}";

    String RESPONSE_MEDIA = "Servindo media: {}";

    String TRANSCODE_MANAGER_DIFFERENT_VIDEO = "O video requisitado é diferente do atual.";

    String TRANSCODE_MANAGER_VIDEO_WITH_DIFFERENT_PARAMS = "O video atual é igual ao requisitado, mas com parâmetros diferentes.";

}
