package com.isamedia.httpserver.impl.client;

import com.isamedia.common.api.Startup;
import com.isamedia.httpserver.api.client.SettingsProperty;
import com.isamedia.httpserver.impl.util.PropertiesReader;
import com.isamedia.media.impl.util.FileUtil;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Created by otavio on 15/10/2016.
 */
@Default
@Startup
@ApplicationScoped
public class DeviceSettingsManager {

    Map<String, DeviceSettings> settings = new HashMap<>();

    @PostConstruct
    private void init() throws IOException, URISyntaxException {
        FileUtil.walk("device", inputStream -> {
            DeviceSettings deviceSettings = new DeviceSettings();
            Properties properties = PropertiesReader.loadProperties(inputStream);

            for (Field field : DeviceSettings.class.getDeclaredFields()) {
                SettingsProperty annotation = field.getAnnotation(SettingsProperty.class);
                if (annotation != null) {
                    Object value = properties.get(annotation.value());
                    try {
                        value = getBooleanValue((String) value);
                    } catch (Exception e) {
                    }

                    try {
                        field.setAccessible(true);
                        field.set(deviceSettings, value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } finally {
                        field.setAccessible(false);
                    }
                }
            }
            settings.put(deviceSettings.getUseragent(), deviceSettings);
        });
    }

    private Boolean getBooleanValue(String value) throws Exception {
        if (value.matches("(?i:^(true|false)$)")) {
            return Boolean.getBoolean(value.toLowerCase());
        } else {
            throw new Exception();
        }
    }

    public DeviceSettings get(String useragent) {
        String matchUserAgent;
        try {
            matchUserAgent = settings.keySet().stream().filter(key -> useragent.contains(key)).findFirst().get();
        } catch (NoSuchElementException e) {
            matchUserAgent = "*";
        }
        return settings.get(matchUserAgent);
    }

}
