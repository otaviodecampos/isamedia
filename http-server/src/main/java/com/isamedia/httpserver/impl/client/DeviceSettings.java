package com.isamedia.httpserver.impl.client;

import com.isamedia.httpserver.api.client.SettingsProperty;
import lombok.Getter;

/**
 * Created by otavio on 15/10/2016.
 */
@Getter
public class DeviceSettings {

    @SettingsProperty("useragent")
    private String useragent;

    @SettingsProperty("transcode.format")
    private String transcodeFormat;

    @SettingsProperty("transcode.video.codec")
    private String transcodeVideoCodec;

    @SettingsProperty("transcode.audio.codec")
    private String transcodeAudioCodec;

    @SettingsProperty("transcode.max.rate")
    private String transcodeMaxRate;

    @SettingsProperty("transcode.buffer.size")
    private String transcodeBufferSize;

}
