package com.isamedia.httpserver.impl.resource;

import com.isamedia.chromecast.impl.Chromecast;
import com.isamedia.chromecast.impl.ChromecastManager;
import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoExternalSubtitle;
import com.isamedia.dlnaserver.impl.vo.VideoControl;
import com.isamedia.dlnaserver.impl.vo.VideoLanguageItem;
import com.isamedia.dlnaserver.impl.vo.VideoLanguageSubtitleItem;
import com.isamedia.media.impl.analysis.type.MediaAnalysis;
import com.isamedia.media.impl.analysis.type.MediaStream;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static com.isamedia.common.api.MediaType.APPLICATION_JSON_UTF8;

/**
 * Created by otavio on 21/10/2016.
 */
@Path("video")
public class VideoControlResource {

    @Inject
    private ChromecastManager chromecastManager;

    @Inject
    private ContentManager contentManager;

    @GET
    @Path("/control/{id}")
    @Produces(APPLICATION_JSON_UTF8)
    public VideoControl get(@PathParam("id") int id) {
        VideoControl videoControl = new VideoControl();

        Video currentVideo = null;
        Chromecast connected = chromecastManager.getConnected();
        if (connected != null && connected.isConnectable() && connected.getChromeCastApi().isConnected()) {
            Integer currentMediaId = connected.getStatus().getMediaId();
            if (currentMediaId != null) {
                currentVideo = contentManager.get(currentMediaId);
                videoControl.setDiscardInitialSeconds(connected.getStatus().getCurrentTime());
            }
        }

        Video video = contentManager.get(id);
        if (currentVideo != null && video.getPath().equals(currentVideo.getPath())) {
            video = currentVideo;
        }

        MediaAnalysis analysis = video.analyse();

        VideoLanguageSubtitleItem defaultSubtitleItem = new VideoLanguageSubtitleItem();
        defaultSubtitleItem.setStream(-1);
        defaultSubtitleItem.setLanguage("Sem Legenda");
        videoControl.addSubtitle(defaultSubtitleItem);

        for (MediaStream stream : analysis.getAudioStreams()) {
            VideoLanguageItem item = new VideoLanguageItem(analysis.getAudioStreams().indexOf(stream), stream.getLanguage());
            boolean current = video.getParams().getAudioStream() == analysis.getAudioStreams().indexOf(stream);
            item.setSelected(current);
            videoControl.addAudio(item);
        }

        for (MediaStream stream : analysis.getSubtitleStreams()) {
            VideoLanguageSubtitleItem item = new VideoLanguageSubtitleItem();
            item.setStream(analysis.getSubtitleStreams().indexOf(stream));
            item.setLanguage(stream.getLanguage());
            boolean current = video.getParams().getSubtitleStream() == analysis.getSubtitleStreams().indexOf(stream);
            item.setSelected(current);
            videoControl.addSubtitle(item);
        }

        for (VideoExternalSubtitle subtitle : video.getExternalSubtitles()) {
            VideoLanguageSubtitleItem item = new VideoLanguageSubtitleItem();
            item.setStream(-1);
            item.setExternalIndex(video.getExternalSubtitles().indexOf(subtitle));
            item.setLanguage(subtitle.getName());
            item.setExternalPath(subtitle.getPath());
            boolean current = video.getParams().hasExternalSubtitle() && video.getParams().getExternalSubtitle().equals(subtitle.getPath());
            item.setSelected(current);
            videoControl.addSubtitle(item);
        }

        if (videoControl.getSelectedAudio() == null && !videoControl.getAudio().isEmpty()) {
            videoControl.getAudio().get(0).setSelected(true);
        }

        if (videoControl.getSelectedSubtitle() == null && !videoControl.getSubtitle().isEmpty()) {
            defaultSubtitleItem.setSelected(true);
        }

        return videoControl;
    }

}
