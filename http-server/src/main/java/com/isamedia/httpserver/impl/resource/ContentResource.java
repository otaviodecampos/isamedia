package com.isamedia.httpserver.impl.resource;

import com.isamedia.dlnaserver.api.contentdirectory.folder.type.Content;
import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.type.folder.Folder;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.vo.VideoCategory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.isamedia.common.api.MediaType.APPLICATION_JSON_UTF8;

/**
 * Created by otavio on 21/10/2016.
 */
@Path("content")
public class ContentResource {

    @Inject
    private ContentManager contentManager;

    @GET
    @Path("/videos")
    @Produces(APPLICATION_JSON_UTF8)
    public Collection<VideoCategory> getVideoListByFolderPath() {
        Map<String, VideoCategory> categories = new HashMap<>();
        for (Content content : contentManager.get()) {
            if (content instanceof Video) {
                Video video = (Video) content;
                if (!video.isDuplicated()) {
                    Folder categoryFolder = content.getParent().getCommonParent();
                    String path = categoryFolder.getPath();
                    if (path != null) {
                        VideoCategory category = categories.get(path);
                        if (category == null) {
                            category = new VideoCategory(categoryFolder.getName(), path);
                            categories.put(path, category);
                        }
                        category.getVideos().add(video);
                    }
                }
            }
        }
        return categories.values();
    }

    @GET
    @Path("/{id}")
    @Produces(APPLICATION_JSON_UTF8)
    public Content getContentById(@PathParam("id") int id) {
        return contentManager.get(id);
    }

    @GET
    @Path("/{id}/children")
    @Produces(APPLICATION_JSON_UTF8)
    public List<Content> getFolderChildren(@PathParam("id") int id) {
        Folder folder = contentManager.get(id);
        return folder.getChildren();
    }

}
