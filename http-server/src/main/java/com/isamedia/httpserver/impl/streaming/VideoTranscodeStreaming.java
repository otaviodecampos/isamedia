package com.isamedia.httpserver.impl.streaming;

import com.google.common.collect.ImmutableList;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoParams;
import com.isamedia.httpserver.api.LogMessage;
import com.isamedia.httpserver.impl.client.RequestDevice;
import com.isamedia.httpserver.impl.util.StreamingUtil;
import com.isamedia.httpserver.impl.util.UnitSizeUtil;
import com.isamedia.media.impl.transcode.VideoTranscoder;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by otavio on 29/08/2016.
 */
@Log4j2
public class VideoTranscodeStreaming {

    private Video video;

    @Getter
    private VideoParams params;

    private VideoTranscoder transcoder;

    private ImmutableList transcodeArgs;

    private int bufferLimitSize;

    private ByteArrayOutputStream buffer;

    private int bufferDiscardedBytes = 0;

    private int streamingId;

    private RequestDevice requestDevice;

    @Getter
    private boolean finished;

    @Builder
    private VideoTranscodeStreaming(RequestDevice requestDevice, Video video, VideoParams params) {
        this.video = video;
        this.params = params;
        this.requestDevice = requestDevice;
        bufferLimitSize = UnitSizeUtil.mbToByte(Integer.parseInt(requestDevice.getDeviceSettings().getTranscodeBufferSize()));
        buffer = new ByteArrayOutputStream(bufferLimitSize);
        transcodeArgs = VideoTranscodeArgs.builder().requestDevice(requestDevice).video(video).params(params).build().getArgs();
    }

    public String getMimeType() {
        return "video/" + requestDevice.getDeviceSettings().getTranscodeFormat();
    }

    public synchronized StreamingOutput stream(String range) throws IOException {
        final int currentStreamingId = streamingId++;
        log.info(LogMessage.START, video.getId(), currentStreamingId);

        if (transcoder == null) {
            transcoder = VideoTranscoder.builder().args(transcodeArgs).build();
            transcoder.run();
        }

        while (buffer.size() < UnitSizeUtil.mbToByte(1)) {
            byte[] bufferSize = new byte[8192];
            int n = transcoder.getInputStream().read(bufferSize);
            if(n > 0) {
                buffer.write(bufferSize, 0, n);
                log.debug(LogMessage.BUFFERING, buffer.size(), video.getId(), currentStreamingId);
            }
        }

        return output -> {
            try {
                int from = 0;

                if (range != null) {
                    int rangeFrom = StreamingUtil.parseRangeFrom(range);
                    from = rangeFrom - bufferDiscardedBytes;
                    if (from < 0) {
                        from = 0;
                    }
                    log.debug(LogMessage.REQUEST_RANGE, video.getId(), from);
                }

                while (true) {
                    byte[] bufferSize = new byte[8192];

                    if (buffer.size() >= bufferLimitSize) {
                        bufferDiscardedBytes += buffer.size();
                        buffer.reset();
                        from = 0;
                        log.debug(LogMessage.BUFFER_RESETS, video.getId());
                    }

                    int n = transcoder.getInputStream().read(bufferSize);
                    if (n > 0) {
                        buffer.write(bufferSize, 0, n);
                    } else {
                        finish(true);
                        break;
                    }

                    if (from < buffer.size()) {
                        log.debug(LogMessage.SENDING_DATA_RANGE, video.getId(), from, buffer.size(), currentStreamingId);
                        byte[] rangeBytes = Arrays.copyOfRange(buffer.toByteArray(), from, buffer.size());
                        for (byte b : rangeBytes) {
                            from++;
                            output.write(b);
                        }
                    }
                }
            } catch (IOException e) {
                log.info(LogMessage.CLOSE, video.getId(), currentStreamingId);
            } finally {
                streamingId--;
            }
        };
    }

    public void finish() {
        finish(false);
    }

    public void finish(boolean force) {
        if (streamingId == 0 || force) {
            finished = true;
            transcoder.finish();

            try {
                buffer = new ByteArrayOutputStream();
                buffer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            log.info(LogMessage.FINISH, video.getId());
        }
    }

}
