package com.isamedia.httpserver.impl.util;

import java.io.*;
import java.util.Properties;

/**
 * Created by otavio on 15/10/2016.
 */
public class PropertiesReader {

    public static Properties loadProperties(InputStream inputStream) {
        return new Properties() {{
            try {
                load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }};
    }

    public static Properties loadProperties(String filePath) {
        return loadProperties(new File(filePath));
    }

    public static Properties loadProperties(File file) {
        return new Properties() {{
            if (file.isFile()) {
                try (final InputStream is = new BufferedInputStream(new FileInputStream(file))) {
                    load(is);
                } catch (final IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }};
    }

}
