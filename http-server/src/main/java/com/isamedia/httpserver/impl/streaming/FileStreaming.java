package com.isamedia.httpserver.impl.streaming;

import lombok.Builder;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by otavio on 28/08/2016.
 */
public class FileStreaming {

    private File file;

    @Getter
    private long startFrom = 0;

    private long endAt = -1;

    @Builder
    private FileStreaming(File file, String range) {
        this.file = file;

        if (range != null && range.startsWith("bytes=")) {
            range = range.substring("bytes=".length());
            int index = range.indexOf('-');
            if (index > 0) {
                try {
                    startFrom = Long.parseLong(range
                            .substring(0, index));
                    endAt = Long.parseLong(range.substring(index + 1));
                } catch (NumberFormatException e) {

                }
            }
        }
    }

    public FileInputStream stream() throws IOException {
        FileInputStream fis = new FileInputStream(file) {
            @Override
            public int available() throws IOException {
                return getAvailableLength();
            }
        };

        fis.skip(startFrom);

        return fis;
    }

    private int getAvailableLength() {
        long length = getEndAt() - startFrom + 1;
        if (length < 0) {
            length = 0;
        }
        return (int) length;
    }

    public long getEndAt() {
        if (endAt < 0) {
            endAt = file.length() - 1;
        }
        return endAt;
    }

}