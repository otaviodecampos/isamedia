package com.isamedia.httpserver.impl.util;

/**
 * Created by otavio on 01/10/2016.
 */
public class UnitSizeUtil {

    public static int mbToByte(int sizeMb) {
        return (sizeMb * 1024) * 1024;
    }

}
