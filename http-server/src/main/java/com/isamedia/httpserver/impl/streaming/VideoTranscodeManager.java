package com.isamedia.httpserver.impl.streaming;

import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoParams;
import com.isamedia.httpserver.api.LogMessage;
import com.isamedia.httpserver.impl.client.RequestDevice;
import lombok.extern.log4j.Log4j2;

import javax.enterprise.context.ApplicationScoped;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by otavio on 01/10/2016.
 */
@Log4j2
@ApplicationScoped
public class VideoTranscodeManager {

    Map<Integer, VideoTranscodeStreaming> streamings = Collections.synchronizedMap(new HashMap<>());

    private Integer currentVideoId;

    public synchronized VideoTranscodeStreaming get(RequestDevice requestDevice, Video video, VideoParams params) {
        VideoTranscodeStreaming videoTranscodeStreaming = streamings.get(video.getId());

        boolean hasCurrentVideo = currentVideoId != null;
        if (hasCurrentVideo) {
            boolean isDiferentVideo = currentVideoId != video.getId();
            boolean hasDiferentParams = params != null && videoTranscodeStreaming != null && videoTranscodeStreaming.getParams() != null && !videoTranscodeStreaming.getParams().equals(params);

            if(isDiferentVideo) {
                log.debug(LogMessage.TRANSCODE_MANAGER_DIFFERENT_VIDEO);
            }

            if(hasDiferentParams) {
                log.debug(LogMessage.TRANSCODE_MANAGER_VIDEO_WITH_DIFFERENT_PARAMS);
            }

            if (isDiferentVideo || hasDiferentParams) {
                streamings.get(currentVideoId).finish(true);
                streamings.remove(currentVideoId);
            }
        }

        currentVideoId = video.getId();

        if (videoTranscodeStreaming == null || videoTranscodeStreaming.isFinished()) {
            videoTranscodeStreaming = VideoTranscodeStreaming.builder().requestDevice(requestDevice).video(video).params(params).build();
            streamings.put(video.getId(), videoTranscodeStreaming);
        }

        return videoTranscodeStreaming;
    }

}
