package com.isamedia.httpserver.impl.resource;

import com.isamedia.dlnaserver.impl.content.ContentManager;
import com.isamedia.dlnaserver.impl.content.folder.FileMediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.MediaDetail;
import com.isamedia.dlnaserver.impl.content.folder.type.Media;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoParams;
import com.isamedia.httpserver.api.HttpHeader;
import com.isamedia.httpserver.api.HttpStatus;
import com.isamedia.httpserver.api.LogMessage;
import com.isamedia.httpserver.impl.client.RequestDevice;
import com.isamedia.httpserver.impl.streaming.FileStreaming;
import com.isamedia.httpserver.impl.streaming.VideoTranscodeManager;
import com.isamedia.httpserver.impl.streaming.VideoTranscodeStreaming;
import com.isamedia.media.impl.poster.PosterManager;
import com.isamedia.media.impl.screen.VideoThumbnailManager;
import lombok.extern.log4j.Log4j2;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Log4j2
@Path("media")
@RequestScoped
public class MediaResource {

    @Inject
    private ContentManager contentManager;

    @Inject
    private MediaDetail mediaDetail;

    @Inject
    private FileMediaDetail fileMediaDetail;

    @Inject
    private RequestDevice requestDevice;

    @Inject
    private VideoTranscodeManager videoTranscodeManager;

    @Inject
    private VideoThumbnailManager videoThumbnailManager;

    @Inject
    private PosterManager posterManager;

    @GET
    @Path("/{id}")
    public Response getFileMediaStream(@PathParam("id") int id, @HeaderParam(HttpHeader.RANGE) String range) throws IOException {
        log.info(LogMessage.REQUEST_MEDIA, id);
        Media media = contentManager.get(id);

        if (media == null) {
            return Response.noContent().build();
        }

        File file = new File(media.getPath());
        FileStreaming fileStreaming = FileStreaming.builder().file(file).range(range).build();

        Response.ResponseBuilder builder = Response.ok(fileStreaming.stream());
        builder.type(fileMediaDetail.getMimeType(file));
        builder.header(HttpHeader.ACCEPT_RANGES, "bytes");
        builder.header(HttpHeaders.CONTENT_LENGTH, file.length());
        builder.header(HttpHeader.CONTENT_RANGE, String.format("bytes %d-%d/%d", fileStreaming.getStartFrom(), fileStreaming.getEndAt(), file.length()));
        builder.header(HttpHeaders.LAST_MODIFIED, new Date(file.lastModified()));
        builder.header(HttpHeader.ETAG, Integer.toHexString((media.getPath() + file.lastModified() + "" + file.length()).hashCode()));
        builder.status(HttpStatus.PARTIAL_CONTENT);
        return builder.build();
    }

    @GET
    @Path("/{id}/transcode")
    public Response getTranscodeMediaStream(@PathParam("id") int id, @HeaderParam(HttpHeader.RANGE) String range, @QueryParam("audioStream") Integer audioStream, @QueryParam("subtitleStream") Integer subtitleStream, @QueryParam("externalSubtitleIndex") Integer externalSubtitleIndex, @QueryParam("discardInitialTime") Integer discardInitialTime) throws IOException, InterruptedException {
        log.info(LogMessage.REQUEST_MEDIA, id);
        Video video = contentManager.get(id);

        if (video == null) {
            return Response.noContent().build();
        }

        VideoParams params = video.getParams().duplicate();
        if (audioStream != null) {
            params.setAudioStream(audioStream);
        }

        if (subtitleStream != null && subtitleStream != -1) {
            params.setSubtitleStream(subtitleStream);
        }

        if (externalSubtitleIndex != null && externalSubtitleIndex != -1) {
            String path = video.getExternalSubtitles().get(externalSubtitleIndex).getPath();
            params.setExternalSubtitle(path);
        }

        if (discardInitialTime != null) {
            params.setDiscardInitialSeconds(discardInitialTime);
        }

        VideoTranscodeStreaming videoTranscodeStreaming = videoTranscodeManager.get(requestDevice, video, params);

        Response.ResponseBuilder builder = Response.ok(videoTranscodeStreaming.stream(range));
        builder.type(videoTranscodeStreaming.getMimeType());
        builder.header(HttpHeader.CONNECTION, "close");
        builder.header(HttpHeader.ACCEPT_RANGES, "none");
        builder.header(HttpHeaders.EXPIRES, 0);
        builder.header("Cache-Control", "no-cache, no-store");
        builder.header("Pragma", "no-cache");
        builder.status(HttpStatus.OK);

        return builder.build();
    }

    @GET
    @Path("/{id}/screen/{height}{half:(/half/[^/]+?)?}")
    public Response getMediaScreen(@PathParam("id") int id, @PathParam("height") int height, @PathParam("half") String half) throws IOException {
        Video video = contentManager.get(id);

        if (video == null) {
            return Response.noContent().build();
        }

        Integer initialSeconds = video.getParams().getDiscardInitialSeconds();
        if (initialSeconds == null) {
            initialSeconds = 0;
        }
        int initialSecondsByDuration = mediaDetail.getDurationInSeconds(video) / 5;
        int startTime = half.equalsIgnoreCase("/half/true") && initialSecondsByDuration >= 0 ? initialSecondsByDuration : initialSeconds;

        Response.ResponseBuilder builder = Response.ok(videoThumbnailManager.get(video.getPath(), height, startTime));
        builder.type("image/jpeg");
        builder.status(HttpStatus.OK);

        return builder.build();
    }

    @GET
    @Path("/{id}/poster")
    public Response getMediaPoster(@PathParam("id") int id) throws IOException {
        Video video = contentManager.get(id);

        if (video == null) {
            return Response.status(404).build();
        }

        byte[] poster = posterManager.get(video.getName());
        if (poster == null) {
            return Response.status(404).build();
        }

        Response.ResponseBuilder builder = Response.ok(poster);
        builder.type("image/jpeg");
        builder.status(HttpStatus.OK);

        return builder.build();
    }

}