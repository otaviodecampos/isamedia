package com.isamedia.httpserver.impl;

/**
 * Created by otavio on 25/08/2016.
 */
public class ServerConstant {

    public static final String SERVER_ADDRESS = "0.0.0.0";

    public static final String PACKAGE_RESOURCE_SCAN = "com.isamedia";

}
