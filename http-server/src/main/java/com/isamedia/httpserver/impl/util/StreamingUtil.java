package com.isamedia.httpserver.impl.util;

/**
 * Created by otavio on 01/10/2016.
 */
public class StreamingUtil {

    public static int parseRangeFrom(String range) {
        range = range.substring("bytes=".length());
        int index = range.indexOf('-');

        if (index > 0) {
            try {
                return Integer.parseInt(range
                        .substring(0, index));
            } catch (NumberFormatException e) {

            }
        }

        return 0;
    }

}
