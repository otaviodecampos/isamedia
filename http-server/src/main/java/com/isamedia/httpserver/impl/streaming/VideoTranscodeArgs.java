package com.isamedia.httpserver.impl.streaming;

import com.google.common.collect.ImmutableList;
import com.isamedia.common.impl.Settings;
import com.isamedia.dlnaserver.impl.content.folder.type.video.Video;
import com.isamedia.dlnaserver.impl.content.folder.type.video.VideoParams;
import com.isamedia.httpserver.impl.client.DeviceSettings;
import com.isamedia.httpserver.impl.client.RequestDevice;
import com.isamedia.media.impl.util.FFmpegUtil;
import lombok.Builder;
import lombok.Getter;

import static com.isamedia.dlnaserver.impl.content.folder.type.video.VideoQuality.*;

/**
 * Created by otavio on 01/10/2016.
 */
public class VideoTranscodeArgs {

    @Getter
    private ImmutableList args;

    @Builder
    private VideoTranscodeArgs(RequestDevice requestDevice, Video video, VideoParams params) {
        Settings settings = requestDevice.getSettings();
        DeviceSettings deviceSettings = requestDevice.getDeviceSettings();

        if(params == null) {
            params = video.getParams();
        }

        ImmutableList.Builder args = new ImmutableList.Builder();

        if (params.hasDiscardTime()) {
            args.add(args("-ss", params.getDiscardInitialSeconds().toString()));
        }

        args.add("-y");
        args.add(args("-v", settings.getTranscodeLog()));
        args.add(args("-i", video.getPath()));

        if (params.hasDiscardTime()) {
            args.add(args("-ss", "0"));
        }

        args.add(args("-f", deviceSettings.getTranscodeFormat()));
        args.add(args("-c:v", deviceSettings.getTranscodeVideoCodec()));
        args.add(args("-maxrate", deviceSettings.getTranscodeMaxRate() + "M"));
        args.add(args("-bufsize", deviceSettings.getTranscodeBufferSize() + "M"));

        args.add(args("-c:a", deviceSettings.getTranscodeAudioCodec()));
        args.add(args("-map", "0:v:" + params.getVideoStream()));
        args.add(args("-map", "0:a:" + params.getAudioStream()));

        if (params.hasSubtitle() && params.getSubtitleGraphic() && params.getQuality() == null) {
            args.add(args("-filter_complex", "\"[0:v][0:s:" + params.getSubtitleStream() + "]overlay[v]\"", "-map", "[v]", "-preset", settings.getTranscodeQualityWithSubtitleGraphicCompression()));
        } else if (params.hasSubtitle() && params.getSubtitleGraphic() && SD.equals(params.getQuality())) {
            args.add(args("-filter_complex", "\"[0:0]scale=-2:576[a];[0:s:" + params.getSubtitleStream() + "]scale=-2:576[b];[a][b]overlay[c]\"", "-map", "[c]", "-crf", settings.getTranscodeQualitySd(), "-preset", settings.getTranscodeQualityWithSubtitleGraphicCompression()));
        } else if (params.hasSubtitle() && params.getSubtitleGraphic() && HD.equals(params.getQuality())) {
            args.add(args("-filter_complex", "\"[0:0]scale=-2:720[a];[0:s:" + params.getSubtitleStream() + "]scale=-2:720[b];[a][b]overlay[c]\"", "-map", "[c]", "-crf", settings.getTranscodeQualityHd(), "-preset", settings.getTranscodeQualityWithSubtitleGraphicCompression()));
        } else if (params.hasSubtitle() && params.getSubtitleGraphic() && FULLHD.equals(params.getQuality())) {
            args.add(args("-filter_complex", "\"[0:0]scale=-2:1080[a];[0:s:" + params.getSubtitleStream() + "]scale=-2:1080[b];[a][b]overlay[c]\"", "-map", "[c]", "-crf", settings.getTranscodeQualityFullHd(), "-preset", settings.getTranscodeQualityWithSubtitleGraphicCompression()));
        } else if (SD.equals(params.getQuality())) {
            args.add(args("-vf", "scale=-2:576", "-crf", settings.getTranscodeQualitySd(), "-preset", settings.getTranscodeQualitySdCompression()));
        } else if (HD.equals(params.getQuality())) {
            args.add(args("-vf", "scale=-2:720", "-crf", settings.getTranscodeQualityHd(), "-preset", settings.getTranscodeQualityHdCompression()));
        } else if (FULLHD.equals(params.getQuality())) {
            args.add(args("-vf", "scale=-2:1080", "-crf", settings.getTranscodeQualityFullHd(), "-preset", settings.getTranscodeQualityFullHdCompression()));
        }

        if (!params.hasSubtitleGraphic() && (params.hasSubtitle() || params.hasExternalSubtitle())) {
            String path = params.getExternalSubtitle() != null ? params.getExternalSubtitle() : video.getPath();
            path = FFmpegUtil.escapeFilePath(path);
            if (!params.hasExternalSubtitle()) {
                path += ":si=" + params.getSubtitleStream();
            } else {
                path += ":charenc=windows-1252";
            }
            args.add(args("-vf", "subtitles=" + path));
        }

        args.add(args("-copyts"));
        args.add(args("-strict", "-2"));
        args.add("-");

        this.args = args.build();
    }

    private String[] args(String... args) {
        return args;
    }

}
