package com.isamedia.httpserver.impl.client;

import com.isamedia.common.impl.Settings;
import lombok.Getter;
import org.glassfish.grizzly.http.server.Request;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.core.Context;

import static com.isamedia.httpserver.api.HttpHeader.USER_AGENT;

/**
 * Created by otavio on 15/10/2016.
 */
public class RequestDevice {

    @Context
    private Request request;

    @Inject
    @Getter
    private Settings settings;

    @Inject
    private DeviceSettingsManager settingsManager;

    @Getter
    private DeviceSettings deviceSettings;

    @PostConstruct
    private void init() {
        deviceSettings = settingsManager.get(request.getRequest().getHeader(USER_AGENT));
    }

}
