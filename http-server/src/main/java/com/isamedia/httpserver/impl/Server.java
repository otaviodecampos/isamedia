package com.isamedia.httpserver.impl;

import com.isamedia.common.api.Startup;
import com.isamedia.common.impl.Settings;
import com.isamedia.media.impl.util.FileUtil;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;
import org.glassfish.jersey.server.ContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

import static com.isamedia.httpserver.impl.ServerConstant.SERVER_ADDRESS;

/**
 * Created by otavio on 22/08/2016.
 */
@Startup
public class Server {

    private static final String APP_WEB_PACKAGE_NAME = "isamedia-app-web-1.0.jar";

    private static final String WEB_CONTEXT = "/";

    private static final String API_CONTEXT = "/api";

    @Inject
    private Settings settings;

    @PostConstruct
    public void init() {
        final ResourceConfig rc = new ResourceConfig().packages(ServerConstant.PACKAGE_RESOURCE_SCAN);

        try {
            HttpServer httpServer = new HttpServer();
            NetworkListener nl = new NetworkListener("listener", SERVER_ADDRESS, settings.getHttpServerPort());
            httpServer.addListener(nl);

            HttpHandler handler = ContainerFactory.createContainer(GrizzlyHttpContainer.class, rc);
            httpServer.getServerConfiguration().addHttpHandler(handler, API_CONTEXT);

            URL appWebPackageUrl = FileUtil.getFile(APP_WEB_PACKAGE_NAME).toURI().toURL();
            ClassLoader classLoader = new URLClassLoader(new URL[]{appWebPackageUrl});
            httpServer.getServerConfiguration().addHttpHandler(new CLStaticHttpHandler(classLoader), WEB_CONTEXT);

            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getServerAddress() {
        return String.format("http://%s:%s/", SERVER_ADDRESS, settings.getHttpServerPort());
    }

}
