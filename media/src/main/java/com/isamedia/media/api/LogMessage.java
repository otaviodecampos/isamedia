package com.isamedia.media.api;

/**
 * Created by otavio on 26/10/2016.
 */
public interface LogMessage {

    String ANALYSIS_FINISH = "Analise de media concluido: {}";

    String TRANSCODE_START_PARAMETERS = "Conversão de media iniciada com os paramêtros: {}";

    String VIDEO_TRANSCODER_FINISH = "Conversão de media finalizada: {}";

    String VIDEO_SCREEN_FINISH = "Video Screen finish: {}";

}
