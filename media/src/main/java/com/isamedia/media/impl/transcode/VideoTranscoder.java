package com.isamedia.media.impl.transcode;

import com.isamedia.media.api.LogMessage;
import com.isamedia.media.impl.RunProcess;
import com.isamedia.media.impl.util.FFmpegUtil;
import lombok.Builder;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.util.List;

/**
 * Created by otavio on 05/09/2016.
 */
@Log4j2
public class VideoTranscoder extends RunProcess {

    @Builder
    protected VideoTranscoder(List<String> args) {
        this.args = args;
    }

    @Override
    protected String getProcessPath() {
        return FFmpegUtil.getFFmpegPath();
    }

    public synchronized boolean run() {
        if (super.run()) {
            log.debug(LogMessage.TRANSCODE_START_PARAMETERS, args);
        }
        return false;
    }

    public synchronized InputStream getInputStream() {
        return process.getInputStream();
    }

    @Override
    public boolean finish() {
        if (super.finish()) {
            log.debug(LogMessage.VIDEO_TRANSCODER_FINISH, args);
            return true;
        }
        return false;
    }

}
