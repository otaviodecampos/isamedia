package com.isamedia.media.impl.analysis;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.isamedia.media.api.LogMessage;
import com.isamedia.media.impl.RunProcess;
import com.isamedia.media.impl.analysis.type.MediaAnalysis;
import com.isamedia.media.impl.util.FFmpegUtil;
import com.isamedia.media.impl.util.StreamUtil;
import lombok.Builder;
import lombok.extern.log4j.Log4j2;

import java.io.Reader;

/**
 * Created by otavio on 10/09/2016.
 */
@Log4j2
public class MediaAnalyzer extends RunProcess {

    private String filePath;

    @Builder
    public MediaAnalyzer(String filePath) {
        this.filePath = filePath;

        ImmutableList.Builder args = new ImmutableList.Builder();
        args.add(new String[]{"-v", "quiet"});
        args.add(new String[]{"-print_format", "json"});
        args.add("-sexagesimal");
        args.add("-show_format");
        args.add("-show_streams");
        args.add(filePath);

        this.args = args.build();
    }

    public MediaAnalysis analyze() {
        if (super.run()) {
            MediaAnalysis analysis;
            try {
                Reader reader = StreamUtil.bufferedReader(process.getInputStream());
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                analysis = mapper.readValue(reader, MediaAnalysis.class);
                reader.close();
                return analysis;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                this.finish();
                log.info(LogMessage.ANALYSIS_FINISH, filePath);
            }
        }
        return null;
    }

    @Override
    protected String getProcessPath() {
        return FFmpegUtil.getFFprobePath();
    }

}
