package com.isamedia.media.impl.analysis.type;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Created by otavio on 11/09/2016.
 */
public enum MediaType {
    VIDEO, AUDIO, SUBTITLE, ATTACHMENT, UNKNOWN;

    @JsonCreator
    public static MediaType factory(String value) {
        MediaType type;
        try {
            type = MediaType.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            type = UNKNOWN;
        }
        return type;
    }

}
