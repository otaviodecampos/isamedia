package com.isamedia.media.impl.screen;

import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.common.impl.PersistenceManager;

import javax.enterprise.context.ApplicationScoped;
import java.util.Map;

/**
 * Created by otavio on 09/10/2016.
 */
@ApplicationScoped
public class VideoThumbnailManager {

    private final String PERSISTENCE_ENTRY_NAME = "video-screen";

    private final String SCREEN_NAME_TEMPLATE = "%s-%s-%s";

    private PersistenceManager persistenceManager = ApplicationManager.getBean(PersistenceManager.class);

    private Map<String, byte[]> screens = persistenceManager.get(PERSISTENCE_ENTRY_NAME);

    public byte[] get(String path, int height, int initialSecond) {
        String screenName = String.format(SCREEN_NAME_TEMPLATE, height, initialSecond, path);
        byte[] screen = screens.get(screenName);
        if (screen == null) {
            screen = VideoScreen.builder().filePath(path).height(height).initialSecond(initialSecond).build().snapshot();
            screens.put(screenName, screen);
            persistenceManager.commit();
        }
        return screen;
    }

}
