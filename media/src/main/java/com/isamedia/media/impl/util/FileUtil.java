package com.isamedia.media.impl.util;

import com.isamedia.common.impl.util.FilenameUtil;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 * Created by otavio on 07/09/2016.
 */
public class FileUtil {

    public static InputStream getFileInputStream(String path) {
        return FileUtil.class.getClassLoader().getResourceAsStream(path);
    }

    public static File getFile(String resource) {
        File file = null;
        URL res = FileUtil.class.getClassLoader().getResource(resource);
        if (res.toString().startsWith("jar:")) {
            try {
                InputStream input = res.openStream();
                file = File.createTempFile("isamedia", FilenameUtil.getFileExtension(resource));
                OutputStream out = new FileOutputStream(file);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = input.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                input.close();
                file.deleteOnExit();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            file = new File(res.getFile());
        }

        if (file != null && !file.exists()) {
            throw new RuntimeException("Error: File " + file + " not found!");
        }

        return file;
    }

    public static String getFilePath(String path) throws URISyntaxException {
        File file = getFile(path);
        return file.getAbsolutePath();
    }

    public static void walk(String resource, WalkAction action) throws IOException, URISyntaxException {
        Path root;
        URI uri = FileUtil.class.getClassLoader().getResource(resource).toURI();

        if (uri.getScheme().equals("jar")) {
            FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
            root = fileSystem.getPath("/" + resource);
        } else {
            root = Paths.get(uri);
        }

        Stream<Path> walk = Files.walk(root, 1);
        for (Iterator<Path> it = walk.iterator(); it.hasNext(); ) {
            Path path = it.next();
            if (path != root) {
                action.on(FileUtil.class.getClassLoader().getResourceAsStream(root.getParent().relativize(path).toString()));
            }
        }
    }

    public interface WalkAction {
        void on(InputStream inputStream);
    }

}
