package com.isamedia.media.impl.analysis;

import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.common.impl.PersistenceManager;
import com.isamedia.media.impl.analysis.type.MediaAnalysis;

import javax.enterprise.context.ApplicationScoped;
import java.util.Map;

/**
 * Created by otavio on 09/10/2016.
 */
@ApplicationScoped
public class MediaAnalysisManager {

    private final String PERSISTENCE_ENTRY_NAME = "mediaAnalysis";

    private PersistenceManager persistenceManager = ApplicationManager.getBean(PersistenceManager.class);

    private Map<String, MediaAnalysis> mediaAnalysis = persistenceManager.get(PERSISTENCE_ENTRY_NAME);

    public MediaAnalysis get(String path) {
        MediaAnalysis analysis = mediaAnalysis.get(path);
        if (analysis == null) {
            analysis = MediaAnalyzer.builder().filePath(path).build().analyze();
            mediaAnalysis.put(path, analysis);
            persistenceManager.commit();
        }
        return analysis;
    }

}
