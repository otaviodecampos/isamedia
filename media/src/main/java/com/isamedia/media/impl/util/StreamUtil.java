package com.isamedia.media.impl.util;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Created by otavio on 07/09/2016.
 */
public class StreamUtil {

    public static void copyInThread(final InputStream inputStream, final OutputStream outputStream) {
        new Thread() {
            public void run() {
                try {
                    copy(inputStream, outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void copy(final InputStream in, final OutputStream outputStream) throws IOException {
        while (true) {
            int bytes = in.read();
            if (bytes < 0) {
                return;
            }
            if (outputStream != null) {
                outputStream.write(bytes);
            }
        }
    }

    public static BufferedReader bufferedReader(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
    }

}
