package com.isamedia.media.impl;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.isamedia.media.impl.util.ProcessUtil;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by otavio on 10/09/2016.
 */
@NoArgsConstructor
public abstract class RunProcess {

    protected List<String> args;

    protected Process process;

    protected abstract String getProcessPath();

    protected boolean run() {
        if (!isStarted()) {
            try {
                ImmutableList command = ImmutableList.builder().add(getProcessPath()).addAll(args).build();
                ProcessBuilder builder = new ProcessBuilder(command);
                process = builder.start();
                ProcessUtil.redirectStreamError(process, System.err);
                return true;
            } catch (Throwable t) {
                Throwables.propagate(t);
            }
        }
        return false;
    }

    public boolean finish() {
        if (!isFinished() && process != null) {
            process.destroy();
            return true;
        }
        return false;
    }

    public boolean isStarted() {
        return process != null;
    }

    public boolean isFinished() {
        return isStarted() && !process.isAlive();
    }

}
