package com.isamedia.media.impl.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by otavio on 19/11/2016.
 */
public class DownloadUtil {

    public static byte[] get(String url) throws IOException {
        return get(new URL(url));
    }

    public static byte[] get(URL url) throws IOException {
        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int n;
        while (-1 != (n = in.read(buffer))) {
            out.write(buffer, 0, n);
        }
        out.close();
        in.close();
        return out.toByteArray();
    }

}
