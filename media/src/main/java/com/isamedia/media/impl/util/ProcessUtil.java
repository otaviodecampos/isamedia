package com.isamedia.media.impl.util;

import java.io.OutputStream;

/**
 * Created by otavio on 07/09/2016.
 */
public class ProcessUtil {

    public static void redirectStreamError(Process process, OutputStream outputStream) {

        StreamUtil.copyInThread(process.getErrorStream(), outputStream);
    }

}
