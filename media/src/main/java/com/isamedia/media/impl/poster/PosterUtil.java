package com.isamedia.media.impl.poster;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by otavio on 19/11/2016.
 */
public class PosterUtil {

    private static String[] patterns = new String[]{"year:\\s\\d{4}\\s", "quality:\\s\\d+p\\s", "por:português"};

    private static String replaceWords = "\\(|\\)|\\[|\\]|\\.|\\s-\\s|bdrip|dublado|-";

    private static String replaceExtensions = "\\smkv|\\savi";

    private static String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

    private static String IMDB_URL = "http://www.imdb.com/find?q=%s&s=tt&ttype=ft&ref_=fn_ft";

    private static String IMDB_RESULT_SELECTOR = "tr.findResult td.result_text a";

    private static String OMDB_URL = "http://www.omdbapi.com/?i=";

    private static String OMDB_JSON_POSTER_ATTR = "Poster";

    public static String getMoviePosterUrl(String filename) throws IOException {
        String year = null;
        String posterUrl = null;
        boolean hasPattern = false;
        boolean foundPoster = false;
        boolean includeYear = true;

        String name = filename.toLowerCase().replaceAll(replaceWords, " ");
        name = name.toLowerCase().replaceAll(replaceExtensions, "");

        while (!foundPoster) {
            for (String pattern : patterns) {
                String patternName = pattern.substring(0, pattern.indexOf(":"));
                pattern = pattern.replace(patternName + ":", "");

                Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(name);
                if (matcher.find()) {
                    String match = matcher.group();
                    name = name.substring(0, name.indexOf(match)).trim();
                    if (patternName.equals("year")) {
                        year = match;
                    }
                    hasPattern = true;
                    break;
                }
            }

            if (!hasPattern) {
                return null;
            }

            String imdbUrl = String.format(IMDB_URL, name.replaceAll(" ", "+"));
            Document document = Jsoup.connect(imdbUrl).userAgent(userAgent).timeout(10 * 1000).get();
            if (!document.select(IMDB_RESULT_SELECTOR).isEmpty()) {
                String imdbId = document.select(IMDB_RESULT_SELECTOR).first().attr("href").replace("/title/", "").replaceAll("/", "");
                imdbId = imdbId.substring(0, imdbId.indexOf("?"));

                String omdbUrl = OMDB_URL + imdbId;
                ObjectMapper mapper = new ObjectMapper();
                Map<String, Object> map = mapper.readValue(new URL(omdbUrl), Map.class);
                posterUrl = map.get(OMDB_JSON_POSTER_ATTR).toString();
            }


            if (posterUrl != null && posterUrl.startsWith("http")) {
                foundPoster = true;
            } else {
                if (includeYear && year != null) {
                    name = name + " " + year;
                    includeYear = false;
                } else {
                    if (name.indexOf(" ") == -1) {
                        break;
                    }
                    name = name.substring(0, name.lastIndexOf(" "));
                }
            }
        }

        return posterUrl;
    }

}
