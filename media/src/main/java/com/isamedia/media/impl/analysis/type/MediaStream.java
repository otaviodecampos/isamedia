package com.isamedia.media.impl.analysis.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.isamedia.media.impl.analysis.type.MediaType.*;

/**
 * Created by otavio on 11/09/2016.
 */
@Getter
public class MediaStream implements Serializable {

    private int index;

    private int width;

    private int height;

    @JsonProperty("codec_type")
    private MediaType type;

    @JsonProperty("codec_name")
    private String codec;

    private Map<String, String> disposition = new HashMap<>();

    private Map<String, String> tags = new HashMap<>();

    public boolean isDefault() {
        return disposition.get("default").equals("1");
    }

    public String getLanguage() {
        String language = tags.get("language");
        return language != null ? language : "Unknown";
    }

    public boolean isVideoStream() {
        return VIDEO.equals(type);
    }

    public boolean isAudioStream() {
        return AUDIO.equals(type);
    }

    public boolean isSubtitleStream() {
        return SUBTITLE.equals(type);
    }

    public boolean isSubtitleGraphic() {
        return "hdmv_pgs_subtitle".equals(codec);
    }
}
