package com.isamedia.media.impl.analysis.type;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by otavio on 11/09/2016.
 */
@Getter
public class MediaAnalysis implements Serializable {

    private List<MediaStream> streams = new ArrayList<>();

    private MediaFormat format = new MediaFormat();

    public MediaStream getVideoStream() {
        return streams.stream().filter(stream -> stream.isVideoStream()).findFirst().orElse(new MediaStream());
    }

    public List<MediaStream> getAudioStreams() {
        return streams.stream().filter(stream -> stream.isAudioStream()).collect(Collectors.toList());
    }

    public List<MediaStream> getSubtitleStreams() {
        return streams.stream().filter(stream -> stream.isSubtitleStream()).collect(Collectors.toList());
    }

}
