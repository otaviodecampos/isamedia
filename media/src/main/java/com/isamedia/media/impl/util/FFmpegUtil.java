package com.isamedia.media.impl.util;

import java.net.URISyntaxException;

/**
 * Created by otavio on 07/09/2016.
 */
public class FFmpegUtil {

    private static final String FFMPEG_PATH = "ffmpeg/ffmpeg.exe";

    private static final String FFPROBE_PATH = "ffmpeg/ffprobe.exe";

    private static String ffmpeg_absolute_path;

    private static String ffprobe_absolute_path;

    public synchronized static String getFFmpegPath() {
        if(ffmpeg_absolute_path == null) {
            try {
                ffmpeg_absolute_path = FileUtil.getFilePath(FFMPEG_PATH);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return ffmpeg_absolute_path;
    }

    public synchronized static String getFFprobePath() {
        if(ffprobe_absolute_path == null) {
            try {
                ffprobe_absolute_path = FileUtil.getFilePath(FFPROBE_PATH);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return ffprobe_absolute_path;
    }

    public static String escapeFilePath(String filePath) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < filePath.length(); i++) {
            char ch = filePath.charAt(i);
            switch (ch) {
                case '\'':
                    sb.append("\\\\\\'");
                    break;
                case ':':
                    sb.append("\\\\:");
                    break;
                case '\\':
                    sb.append("/");
                    break;
                case ']':
                case '[':
                case ',':
                case ';':
                    sb.append("\\");
                default:
                    sb.append(ch);
            }
        }

        return sb.toString();
    }

}
