package com.isamedia.media.impl.screen;

import com.google.common.collect.ImmutableList;
import com.isamedia.media.api.LogMessage;
import com.isamedia.media.impl.RunProcess;
import com.isamedia.media.impl.util.FFmpegUtil;
import com.isamedia.media.impl.util.StreamUtil;
import lombok.Builder;
import lombok.extern.log4j.Log4j2;

import java.io.ByteArrayOutputStream;

/**
 * Created by otavio on 10/09/2016.
 */
@Log4j2
public class VideoScreen extends RunProcess {

    private String filePath;

    @Builder
    public VideoScreen(String filePath, int height, int initialSecond) {
        this.filePath = filePath;

        ImmutableList.Builder args = new ImmutableList.Builder();
        args.add(new String[]{"-y"});
        args.add(new String[]{"-v", "error"});
        args.add(new String[]{"-ss", String.valueOf(initialSecond)});
        args.add(new String[]{"-i", filePath});
        args.add(new String[]{"-frames:v", "1"});
        args.add(new String[]{"-f", "image2pipe"});
        args.add(new String[]{"-vf", "scale=-2:" + height});
        args.add(new String[]{"-"});

        this.args = args.build();
    }

    public byte[] snapshot() {
        if (super.run()) {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                StreamUtil.copy(process.getInputStream(), output);
                return output.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                this.finish();
                log.info(LogMessage.VIDEO_SCREEN_FINISH, filePath);
            }
        }
        return null;
    }

    @Override
    protected String getProcessPath() {
        return FFmpegUtil.getFFmpegPath();
    }

}
