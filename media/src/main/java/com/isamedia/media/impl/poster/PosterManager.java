package com.isamedia.media.impl.poster;

import com.isamedia.common.impl.ApplicationManager;
import com.isamedia.common.impl.PersistenceManager;
import com.isamedia.media.impl.util.DownloadUtil;
import lombok.extern.log4j.Log4j2;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.util.Map;

/**
 * Created by otavio on 09/10/2016.
 */
@Log4j2
@ApplicationScoped
public class PosterManager {

    private final String PERSISTENCE_ENTRY_NAME = "video-posters";

    private PersistenceManager persistenceManager = ApplicationManager.getBean(PersistenceManager.class);

    private Map<String, byte[]> posters = persistenceManager.get(PERSISTENCE_ENTRY_NAME);

    public byte[] get(String videoName) {
        byte[] poster = posters.get(videoName);
        if (poster == null) {
            try {
                String posterUrl = PosterUtil.getMoviePosterUrl(videoName);
                if (posterUrl != null) {
                    poster = DownloadUtil.get(posterUrl);
                }
            } catch (IOException e) {
                log.debug(e);
            }
            if(poster != null) {
                posters.put(videoName, poster);
                persistenceManager.commit();
            }
        }
        return poster;
    }

}
