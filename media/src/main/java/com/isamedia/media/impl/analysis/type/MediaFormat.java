package com.isamedia.media.impl.analysis.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by otavio on 11/09/2016.
 */
@Getter
public class MediaFormat implements Serializable {

    @JsonProperty("nb_streams")
    private int streams;

    private String duration = "0:00:00.000000";

    private Long size;

    @JsonProperty("bit_rate")
    private long bitRate;

}
